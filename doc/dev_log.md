# 2019-11-01
#### Finish:
* support different value length in benchmark.
* udpate benchmark result.

# 2019-10-31
#### Finish:
* improve benchmark client to give a as large as possible workload to server:
  * using one entrusting thread.
  * entrusing request in a batch manner rather than one-by-one.
* found that in grpc a client can't always get informed when it cannot connect to the server if there is no timeout value set for ` ::grpc::ClientContext`.

# 2019-10-29
#### Finish:
* found I've fallen into a common trap when doing benchmark: `latency` will increase drastically under a large load, because it contains the `waiting time`! 

# 2019-10-28
#### Finish:
* found the high latency is due to the benchmark client's inaccurately using the number client entrusting number. WTF...
* compiles on MAC.
* improve: make each client CQ corresponding to one notify thread to get around the entrusting problem.
* remove some debug logics.

# 2019-10-26
#### Finish:
* fix unit test issues in `TestConnPool` & `TestFollowerEntity`.
* calculating per-thread latency in `BenchmarkClient`.

# 2019-10-25
#### Finish:
* improve unit for `LockFreeDeque` & `LockFreeQueue`.

#### Bugfix:
* overlapping status issue in `LockFreeQueue<T>::Pop`.

# 2019-10-22
#### Finish:
* after digging a lot, finally found it's the `#followers` that acting as the critical factor for latency.

# 2019-10-16
#### Finish:
* adjust some md documents' formats.
* improve: any initialization work should be done before `this->m_async_service->RequestXX` in the Request construstors.
* remove and adjust some comments.
* found that a raw grpc broadcasting model didn't get a poor performance result, so something must be wrong in aurora's broadcasting model.

# 2019-10-13
#### Finish:
* remove some comments.
* add `_SVC_APPEND_ENTRIES_TEST_` for `AppendEntries`.
* modify `restart_*.sh` to make it work under linux.
* there is no difference on performance when testing under Linux. The grpc framework still plays the critical factor for performance bottleneck, disappointed.

#### Bugfix:
* new `Write` objects concurrently is not thread safe under linux, thus we must synchronize different `new` operations or simply use `tcmalloc`, I choose the latter.

# 2019-10-12
#### Finish:
* found gcc8.3.1 doesn't support `std::atomic` well, check [this](https://stackoverflow.com/a/26945986/2886435) and [this](https://github.com/riscv/riscv-gnu-toolchain/issues/183#issuecomment-253721765).
* compiles on linux with gcc 8.3.1.

# 2019-10-11
#### Finish:
* some conclusions about performance:
  * bottleneck is on the server side and it's the grpc framework which has a inherent bottleneck of ~2w throughput & ~500ms latency.
  * follower is not fully saturated.
  
* add some trace logs and a new vlog level of 90.
* udpate docs.

* refactor:
  * modify latency counter related code.
  * use a const parameter reference form for `EntrustRequest`.

# 2019-09-30(2)
#### Finish:
* found [this APPLE clang shit](https://stackoverflow.com/questions/33603027/get-apple-clang-version-and-corresponding-upstream-llvm-version/36000632?stw=2#36000632). And `Apple LLVM version 10.0.0 (clang-1000.10.44.4)` doesn't support the `Return type deduction for normal functions` feature in c++14 even though [this](https://en.cppreference.com/w/cpp/compiler_support) said it does, good chaos.
* using a general CQ instead of a `ServerCompletionQueue` for client reacting.
* adjust cq & #threads default configs.
* add some testing code.

#### Bugfix:
* `CutHeadEmpty`'s shared_ptr that refs to `Write` should initialized with client's shared_ptr rather than `Write` own's.

#### Note:
* benchmark result is highly related to the way of how the benchmark client working: a one-cq vs one-thread pattern can get a much better result than others. Seems that [this note](https://grpc.github.io/grpc/cpp/md_doc_cpp_perf_notes.html) works for client, but now for server under my test:
> Right now, the best performance trade-off is having numcpu's threads and one completion queue per thread.

# 2019-09-30(1)
#### Finish:
* modify some default config values.
* remove `Write::AppendBinlog::_mutex_lock` for optimization.

#### Bugfix:
* client cq & threads didin't get invoked.
* lacking of `volatile` keyword for `CommonView::m_running_flag`.

# 2019-09-29
#### Finish:
* add some config items for unit test.
* improve : replace `std::atomic_flag` with `std::atomic<bool>`.
* add & remove some debug logs.

# 2019-09-28(2)
#### Finish:
* found an important yet a little bit confusing restrict on using async stream(i.e, `ClientAsyncReaderWriter`):
  it should waiting until the previous ` stream_->Write` to be done, before issuing the next ` stream_->Write`, some discussions can be found [here](https://github.com/grpc/grpc/issues/7659) and [here](https://github.com/grpc/grpc/issues/17669#issuecomment-453276385).

# 2019-09-28(1)
#### Finish:
* remove compile warnings under x64.
* remove unnecessary comments.
* add latency statistc for leader service.
* modify some default config items to more proper values.
* add some config items for unit test.
* add TestBinlog.Perf unit test.
* do not use `FollowerView::m_binlog_info_mutex.` anymore, relies on the timeout mechanism of bg threads to detect the adjacent events of the log entries which are wrongly pushed to bg queues.
* remove unused variables of `Write::m_snapshot` && `StorageMgr::GetSlice::max_suffix`.
 
#### Bugfix:
* add the missing `volatile` to some multiple threads accessing POT type variables.
* `FollowerEntity::m_last_sent_committed.load` related issues.
* a `CHECK` issue in `AppendEntries::ProcessAdjacentLog`.
* calling issue of `TrivialLockSingleList<T>::InsertTracker::InsertRaw`.

# 2019-09-24
#### Finish:
* performance optimization:
  * using `static_cast` in upcasting scenarios.
  * adjust benchmark client payload distribution : smaller payload for each thread is better.

* refactor:
  * merge some functions.
  * some benchmark code structures.
  * simplify statistic logis in `BenchmarkBase::BenchmarkBase()`.
  * naming conventions.

#### Bugfix:
* benchmark statistic issues.

# 2019-09-22
# Note:
* grpc throughput is also poor under linux: ~3w/s, there is a [snapshot](https://console.cloud.tencent.com/cvm/snapshot/detail/snap-6n8go437?regionId=5) in tencent cloud demonstrating this.

# 2019-09-21
# Note:
*  `brpc`'s benchmark for grpc is [here](https://github.com/apache/incubator-brpc/blob/master/docs/cn/benchmark.md): only ~4.5w/s
![grpc throughput through baidu' test](https://github.com/apache/incubator-brpc/blob/master/docs/images/qps_vs_reqsize.png?raw=true)

# 2019-09-20
#### Finish:
* add: follower service pure benchmark client.

#### Bugfix:
* stack overflow issue due to an inappropriate resursive invoking.
* bg request timeout using a wrong config.
* didn't wait on the right node in `TrivialLockSingleList<T>::CutHead::WaitForListClean`.

# 2019-09-19
#### Finish:
* refactor: MPMC use a `std::condition_variable` to be aware of data coming event, abandon its previous way.

# 2019-09-18
#### Finish:
* compiles `DEBUG` & `RELEASE` versions on MAC.
* reduce #thread spawned for `LockFreePriotityQueue`.
* delete some comments & the `guid.config` file.

#### Bugfix:
* `GetLocalIPs` didn't work on mac. Added an additional config item to explicitly tell the local address.
* `Write::m_write_stage` wasn't initialized.
* didn't consider the `pre_log_id()` case in `StorageMgr::ConstructFromBinlog`.

# 2019-09-17
#### Finish:
* add:
  * a explicit `CHECK()` when no `CommitEntriesAsyncClient` available, like what it is for `AppendEntriesAsyncClient`.
* remove some comments.
* not a bad benchmark result(~2000 qps all in windows) with config: `Release GLOG_v=1`.

#### Bugfix:
* `m_last_sent_committed` didn't properly get updated in `Write::ReplicateDoneCallBack`.
* when there is valid binlog files exist, storage didn't get correctly initialized.

# 2019-09-15
#### Finish:
* add:
  * send non-op requests after a new leader elected out.
  * renaming binlog file name when server role changed.
  * make `BinLogOperator` & `StorageMgr` can be instantiated & add a gloabl object respectively.
  * some debug loggings.

* improve:
  * someloggings & trivial logcis in unit test.
  * `ProcessOverlappedLog`.
  * some trivial logics in `StateMgr` & `StorageMgr` & `GlobalTimer`.
  
* refactor:
  * sending heartbeat logic in `LeaderView`.
  * add `Write::AddResyncLogTask()`.
  * judging boundary logic in reverting binlog moved into `RevertLog`.

#### Bugfix:
* issues of judging the first request after a new leader elected.
* no volatile keyword describing some multiple-thread RW variables.
* thread resource in election wasn't properly released.
* didn't include the leader itself to the succeed number before replicating to the whole cluster.
* didn't start a resync-log task if one follower times out.
* delete elements didn't get properly dealt with in `TrivialLockDoubleList`.
* an inheritance issue in `SingleListNode`.

#### Note:
* found [a good explantion](https://www.codeproject.com/Articles/808305/Understand-std-atomic-) about why `spurious fail` of `compare_exchange_weak()` occurs on some platforms.

# 2019-09-07
#### Finish:
* refactor:
  * use a non-static semantic for `StorageMgr` & `BinLogOperator`.
    * add static global instances.
  * move `StorageMgr` initialization from `CommonView` to `GlobalEnv`.

* add:
  * `BinLogOperator::DeleteOpenBinlogFile`.
  * common macros.
  * forced CHECK to explicitly tell the client pool enhausted issue.

* improve:
  * return a `OVERSTEP` error in `AppendEntries` only when there is a real log conflict rather than the `input_logs` go ahead over the `LCL`.

#### Bugfix:
* `StorageMgr::m_last_committed` didn't get correctly initialized for didn't parsing the binlog file during startup.

# 2019-09-04
#### Note:
* encountered a volatile issue for `GlobalEnv::m_cq_fully_shutdown`, see the assembler code in `Release` version **WITHOUT** keyword `volatile`:
![release-non-volatile](https://live.staticflickr.com/65535/48676986612_0c1c985cfd_o.png)

We cannot get updated `m_cq_fully_shutdown` value since it doesn't read it from memory everytime using it. Then see `Release` version **WITH** keyword:
![release-volatile](https://live.staticflickr.com/65535/48676816266_31a82011e2_o.png)

This time we can get the updated `m_cq_fully_shutdown` value as it was reloaded from memory everytime using it. `Debug` version can also make a guarantee of this even **WITHOUT** keyword `volatile`:
![debug-non-volatile](https://live.staticflickr.com/65535/48676986682_2be27404fe_o.png)

Meanwhile, other POT static variables like `GlobalEnv::m_running`, this is the `Release` version **WITHOUT** keyword `volatile`:
![release-non-volatile-other](https://live.staticflickr.com/65535/48676986537_af44ef597f_o.png)

So, for all the static variables, only found `GlobalEnv::m_cq_fully_shutdown` need to be volatiled.

#### Bugfix:
* multiple thread accessing issue in `AppendEntries::ProcessDisorderLog`.
* when to finish the client benchmarking threads issue in `DoBenchmark`.
* volatile issue in `GlobalEnv::StopGrpcService`.

# 2019-09-02
#### Finish:
* add some configs for unit test.
* modify:
  * some default config value suitting for the `Release` version.
  * `LockFreeHash` unit test structures.
  * some naming conventions.
  * remove some logging comments.
  
* improve:
  * the whole `OperationTracker` solution especially on how to properly using `LockFreeHash` in the tracker.
  
* refactor:
  * the Insert(Raw) operations in `TrivialLock*List`.
  
#### Bugfix:
* didn't judge reach the list head-end in `TrivialLock*List::Delete()`.
* the GC thread in some unit tests didn't quit after test ending.
* didn't consider the case of producing threads overlap consuming threads in `LockFreeQueue`.

# 2019-08-31
#### Finish:
* Got a cool [approach](https://stackoverflow.com/questions/15374841/c-template-partial-specialization-member-function/42206166?stw=2#42206166) for solving the *partial specialization member function* problem.

# 2019-08-27
#### Finish:
* modify:
  * a control flag of `ReSyncLogContext::m_hold_pre_lcl` indicating whether necessary to call `BinLogGlobal::m_instance.SubPreLRLUseCount`.
* add:
  * `_GLOBAL_TEST_` for unit test.
* improve:
  * reduce the number of `CommitEntriesAsyncClient`.
* compiles on MAC.

#### Bugfix:
* `Swap(_binlog_item.mutable_entity` cause coredump issue.
* didn't call `mutable_write_op` in `LeaderView::SyncDataCB`.

# 2019-08-26
#### Finish:
* refactor:
  * some naming conventions adjustment.
  * `LastlogResolve`, a new mechanism to waiting all logs done before the `last_released_guid`.
  * reduce log amount in `Write::ProcessCutEmptyRequest`.
  * `AppendEntries::ComposeInputLogs` & `AppendEntries::ProcessDisorderLog` & `Write::ProcessCutEmptyRequest`.

#### Bugfix:
* didn't consider `AppendEntries` could precede LRL in the follower side, and corresponding logics on both leader & follower sides.
* didn't return a result to client when leader cannot entrust a majority clients in phaseI.
* `TrivialLockSingleList<T>::SetEmpty` didn't set tail to null.

# 2019-08-24
#### Finish:
* refactor:
  * solution for CGG problem : determine the result for the [implicit] failed requests in the background iterating threads instead of `LastlogResolve`.
  * remove `Write::EndDetermined`.
  
* modify:
  * some logs' logging position.
  * add `recheck` in `Write::CutEmptyRoutine`.
  * add `TrivialLockSingleList::SetEmpty`.

#### Bugfix:
* `release_write_op` related issues.

# 2019-08-23
#### Finish:
* add:
  * more detailed log for CQ false result in bidirectional request.
  * some config items for unit test.
  * test case for follower service.
  * `MemoryTable::IterateByVal`.
  * found some TODO list for storage and write them down in the form of comments.
  
* modify:
  * move `zero_log_id` to `CommonView`.
  * some default config values.
  * add timeout in follower service unit test.

* refactor:
    * add `max_log_id` in `CommonView`.
    * do not compare raw KV strings in `MemoryLogItemFollower` to support memory log overwritting.
    * `Write::LastlogResolve`.
    * `MemoryTable::IterateByKey`.
    * use a `shared_ptr` rather than a raw `std::string` in `RaftCore::Storage::HashValue`.
    
#### Bugfix:
* `StorageMgr::GetSlice()` didn't iterate the memory table.
* wrongly called `release_write_op` in it's still in use for `this->m_shp_entity` in `Write` request.
* logging issue in `Write::UpdateServerStatus`.

# 2019-08-21
#### Finish:
* remove some unused configs.
* adjust some default config values to be more applicable.
* remove some comments.
* modify the test framework for leader service test, allowing for testing seperate server processes.
* modify the way of how `m_last_sent_committed` get updated : eliminate the use of `m_last_sent_committed_mutex`.
* log more detailed info(+ the ret_code) when pushing bg tasks.
* no need to return after `GuidGenerator::GenerateGuid`, it can never fail in current design.

#### Bugfix:
* leader didn't push the cutempty requests to the bg cutempty list.
* follower didn't delete elements from the pending list when they get timeout.
* `RESYNC-LOG` & `RESYNC-DATA` re-entry issue.
* didn't set `UnorderedSingleListNode::m_data` to `nullptr` before deleting its coresponding node.

# 2019-08-20
#### Finish:
* optimize: reduce #IO operations in storage.

# 2019-08-19
#### Finish:
* improve:
  * using `dynamic_cast` to fit the `downcast` scenarios.
  * use some uniform configs to replace previous tedious config items.
   
* add:
  * some compile time assertions.
  * some config for unit tests.
  
#### Bugfix:
* `OwnershipDelegator::ResetOwnership` didn't get invoked before request object's initialization.
* threads that iterating disorder list can get overlapping items, resuling into a coredump issue finally.
* memory leak issues:
  * `shared_ptr`'s deleter issue in `OwnershipDelegator`.
  * in storage GC process.
  * in memory table dumping process.

# 2019-08-10
#### Note:
* if don't specify the `deleter` parameter of `std::shared_ptr<T>::reset()`, a default `delete expression` will be used.

# 2019-08-09
#### Note:
* it's important to notice the existance of `downcast` and `sidecast`(or `crosscast`), check [this](https://en.cppreference.com/w/cpp/language/dynamic_cast) for standard details and [this](https://stackoverflow.com/a/51669399/2886435) for examples.

# 2019-08-07
#### Finish:
* solve the problem of client cannot fetch result ASAP : the entrusting job in very slow(~2.5/ms).
* improve : connection perlink and channel pool size related logics.

# 2019-08-06
#### Finish:
* refactor:
  * using a CQ pair instead of a single CQ for server adopt incoming requests, according to [this](https://groups.google.com/forum/#!topic/grpc-io/V4NAQ77PMEo), the pair of CQ is desined as:
    * one is for notify the server start a call.
    * the other is for serving the subsequent read & write.
  * the structure of starting a grpc service in `GlobalEnv::StartGrpcService`.
  * add `RequestBase<T>::Initialize()` to remove some redundant code.
  
#### Bugfix:
* concurrent accessing problem with `GlobalEnv::m_released_cq_idx;`

#### Note:
* there are distinguished CQ on the server side that one is for `notifying a call is started` and the other one is for `subsequent read & write`, according to [this](https://groups.google.com/forum/#!topic/grpc-io/V4NAQ77PMEo), giving different CQs can slightly improve throughput under my test.

# 2019-08-05
#### Finish:
* optimize: use a proper memory order of `release-consume` for `BinLogGlobal::m_instance.m_last_logged`.
* use template template parameter for `CommonView::InstallGC`.
* refactor:
  * `config.*` file format.
  * replace `TrivialLockDoubleList` with `TrivialLockDoubleList` in `FollowerView::m_disorder_garbage` and `LeaderView::m_pending_list`.
  * use a `thread-per-CQ` semantic rather than the previous `thread-jumping-beteween-CQs` semantic.
* basically finished `developer_guide.md`.

#### Bugfix:
* stage issue in `BidirectionalAsyncClient<T,R,Q>::React`.
* using a wrong upper boundary of cuthead in the recheck logic after cuthead from ` LeaderView::m_pending_list` in `Write::CutEmptyRoutine`, and the corresponding logics in ` FollowerView::m_disorder_list`.

# 2019-08-04
#### Finish:
* add new design and implementation details to the `developer_guide.md`.

#### Note:
* adding #CQ at client side doesn't help enhance throughput in the pure grpc client-server benchmark.

# 2019-07-31
#### Finish:
* add the `TrivialLockSingleList<T>` class to optimize scenarios where only `CutHeadByValue` is needed.
* add some unit test macros to give a fine-grained control over the testing logics.
* add & modify some config options to fit the new features in this commit.
* add some debug logs.
* add `upsert` in `LockFreeHash`.
* refactor:
  * add some of the dedicated iterating background threads related logic to `CommonView`.
  * naming conventions of: `DisorderMessageContext` & `LockfreSingleList` & `TrivialLockQueue`, etc.
  * extract some common classes into [list]common files in the `data structure` directory, like `DoubleListTypeBase` & `OperationTracker`, etc.
  * benchmark testing client to give more detailed result and a stronger pressure(by using multiple client-side CQs) to the backend server.
  * single & double list testing client to debug things.

#### Bugfix:
* issues caused by the unfinished inserting and deleting opertions in a cut off list.
* reassign the value of `std::shared_ptr<>` when encounting equal items in `List::Insert()`.

#### Note:
* if an empty deleter is assigned to an object(says `shp`) of `std::shared_ptr<>`, then the object cannot get its destructor called if it was managed by `shp` and its spawners. 
* the deleter will be transferred while using `std::shared_ptr<T>::operator=` to copy `std::shared_ptr<>` objects, check [this](https://stackoverflow.com/a/31365247/2886435).

The above two rules are literally describing the same thing.

# 2019-07-22
#### Finish:
* basically finish the new version of `developer_guide.md`.

# 2019-07-21
#### Finish:
* compiles on mac.
* improve : `_need_entrust` related logic in `Write::ReplicateDoneCallBack`.
* bugfix:
  * the limitation of `group commit` isn't implemented in a proper way, could resulting to lose commit request to certain followers.
* add new contents to documents.

# 2019-07-20
#### Finish:
* remove some unused config items.
* adjust some naming conventions.
* refactor:
  * leader use a dedicated thread to iterating over the `CutEmpty` requests.
  * fine grain the `Write` process into smaller steps to fit the above modification.
  * replace `int` with `FinishStatus` to indentify the finished/unfinished results.
* improve:
  * notifying the CV which waited by the `Disorder Message Routine` after successfully appended the binlog.
* add new contents to documents.

# 2019-07-19
#### Finish:
* refactor:
  * use `std::deque` instead of `std::vector` as the underlying storage layer of `std::priority_queue`.
* naming conventions.

# 2019-07-16
#### Note:
* Yet found [another grpc implicit behaviors](https://github.com/grpc/grpc/issues/19658) **AGAIN**!!!

# 2019-07-16
#### Finish:
* don't use background threads to waiting on the CV for disorder messages, it will causing background threads resource exhausted, spawning a dedicated threads to iterate over the disorder list, can take the corresponding actions. This modification contains:
    * add a new `DisorderMessageContext` task type.
    * add the `OwnershipDelegator` to the `AppendEntries` request class, since the disorder message need to extend its lifetime.
    * add the `FollowerView::m_disorder_list` list and its corresponding `FollowerView::m_disorder_garbage` garbage list and recycle processes.
    * modify the `FollowerView::m_cv` to being used for the disorder list empty status notifications.
    * add a dedicated background thread to iterate over the disorder list and its corresponding elegant exiting steps.
* `TrivialLockDoubleList::Insert()` : replace the old element with the new one in conflict detected.
* do not delete the timeout elements in the `FollowerView::m_phaseI_pending_list`, letting them to be overwritten be the newly coming logs after a log reverting happened, or correctly processed(get transferred to `FollowerView::m_phaseII_pending_list`) with the help of its succeeding logs, in which case it's actually a `false negative`.


# 2019-07-13
#### Finish:
* modify:
  * some default config values.
  * on the leader side, add dedicated CQs for client reacting. 
  * test case to be more accurate for logging the debug infos.
* refactor:
 * some naming conventions.
 * leader's behavior after get a determined result from a log entry changed: a timeout entry won't influence it's subsequent logs any more.
 * add client reacting job to background threads on the follower side.

#### Bugfix:
 * `ServerStatus` shouldn't be a request-dependent variable.

#### Note:
* found [another grpc implicit behaviors](https://github.com/grpc/grpc/issues/19573).

# 2019-07-09
#### Note:
* `CompletionQueue::Next()` method will return a `false` value for the output parameter `ok`, if it has been timeout when calling the `ServerAsyncResponseWriter::Finish()` method on the server side. But how does the server detect the timeout event made me puzzled, details are [here](https://mail.google.com/mail/u/0/#sent/KtbxLzfldxWXrHDBNntHfWrBBVdFxZsxPg).

# 2019-07-04
#### Finish:
* benchmark testing framework for both `LeaderService` & `FollowerService`.
* modify: 
  * some unit test config items' name.
* improve:
  * a new locking mechanism to solve the mutex heavily contenting problem under huge workload which will causing low throughtput for the follower service.
  * logs.

#### Note:
* Can't waiting on a same `condition variable` but using different `mutex`, will leading undefined behavior, check [this](https://wiki.sei.cmu.edu/confluence/display/c/POS53-C.+Do+not+use+more+than+one+mutex+for+concurrent+waiting+operations+on+a+condition+variable) and [this](https://books.google.com.hk/books?id=r_djDAAAQBAJ&pg=PA176&lpg=PA176&dq=c%2B%2B+condition+variable++%22different+mutex%22&source=bl&ots=2tvfkfX6g0&sig=ACfU3U1kNvdNX9zEVjbw1WvVUZo9UNxxFg&hl=en&sa=X&ved=2ahUKEwiBwI6t7JvjAhUS7bwKHcraBTsQ6AEwBHoECAgQAQ#v=onepage&q=c%2B%2B%20condition%20variable%20%20%22different%20mutex%22&f=false), [some article](http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2007/n2406.html) says *There exist use cases for waiting on the same condition variable with multiple mutexes*, but note there is a precondition *The simplest example involves only one waiting thread which uses a different mutex on each wait*.

# 2019-07-03
#### Finish:
* follower service benchmark. The result is okay for `Debug` version : ~3.6k~5.5k TPS.
* add a template service benchmark base class.
* remove some compile warnings.
* modify:
  * repalce `LOG(INFO)` with `VLOG` in high frequence invoked interfaces.

# 2019-07-02
#### Finish:
* so called `memory leak` : there is no leak, the unexpected memory usage are caused by:
  * windows page allocation policy.
  * `HashNode<>` class itself.
  
  > leaking contribution from `std::shared_ptr<>` can be `basically` eliminated, it's a very tiny factor which can influencing the memory useage under my test in `TestMeta.MetaLeak`.

* modify:
  * using `||` rather than `&&` in binlog exceeding limit judge.
  
* improve:
  * `_one_slot_size` calculation more accurate.
  * using `nullptr` as the default parameter value in `~LockFreeHash::Insert()` getting rid of potential allocating operations.
  * 

#### Bugfix:
* `Explicitly specialized members need their surrounding class templates to be explicitly specialized as well`,  check [this](https://stackoverflow.com/a/5513109/2886435).

# 2019-07-01
#### Finish:
* refactor:
  * `ElectionMgr`, remove some unnecessary variables & logcis, etc.
  * add `ElectionMgr::Reset()` to make election logic more clear.
* improve:
  * use `ElectionMgr::m_phase*_task`, thus only sending `VOTE` request to the nodes whose `PRE-VOTE` phase succeed.
  * solve compile warnings.
  * `TestMember::SetStoragePoint()` add `backoff` parameter to give fine-graind control.
  * remove the the limitation of `only resync logs when there are enough entries before ID-LCL` when resyncing logs.

#### Bugfix:
  * calculation issue in `TwoPhaseCommitContext::PhaseState::JudgeClusterDetermined()`.
  * new nodes can't take part in immediate election after a leader gone away membership changing happened.

# 2019-06-29
#### Finish:
* refactor:
  * naming conventions for `typedef`.
  * use a dedicated struct `TwoPhaseCommitBatchTask<T>` to represent the bach 2pc tasks, abandoning the original `todo_set`.
* improve:
  * RW lock usage standard.
  * add some name alias.

#### Bugfix:
* deadlock issue.
* `ResetMemchgEnv()` didn't get called when it should.
* statistic issues in `MemberMgr`.
* statistic issue in `TwoPhaseCommitContext::JudgeAllFinished()`.

# 2019-06-28
#### Finish:
* refactor:
  * remove some useless logs.
  * add `_ELECTION_TEST_` macro.
  * rename config item names in `election.config`.
  * a simpler and more clear `IncreaseToMaxterm` implementation.
* improve:
  * more strict rules for using RW lock in election related logics.
  * unit test for election.
  
#### Bugfix:
* `StringSplit` output result disorder issue.
* `term` didn't go back when it needs to : the rewind of jumping logic.
* `callback` parameter issue in election clients.
* `InitGrpcEnv()` called too many times.


# 2019-06-27
#### Finish:
* refactor:
  * wrapped `UnorderedSingleListNode` within `LockFreeUnorderedSingleList`.
  * the whole GC mechanism toward `MemoryLogItemBase`.
  * rename `TrivialLockList` to `TrivialLockDoubleList`.
  * separate `MemoryLogItemBase` subclasses' constructor and destructor definitions from their `.h` files.
  * header file macro names.
  * `LockFreeDeque` 
    * add some test-only functions.
    * a brand new popping mechanism: add a `fake-node` to keep the lockfree manner.
* add:
  * `macro_manager.h` to control test macros in fine-grained.

#### Bugfix:
* `_FILE_META_DATA_MAGIC_STR_` length issue in `FileMetaData::GenerateBuffer()`.
* `LockFreeDeque<T>::GC()` didn't work.
* commit index issue in the unit test of `TestFollowerService.ConcurrentOperation)`.
* ownership issue when invoking `m_storage.Set()`.

# 2019-06-22
#### Finish:
* refactor:
  * resolve some compile warnings.
  * the whole client classes hierarchy. Simplified code in `client_impl.cc`.
  * separated `ChannelPool` from the `ClientPool`.
  * removed unnecessary legacy codes.
  * remove redundant `WriteProcessStage` enum values.
  * remove debug logs.
  * combine 3 'ownership classes' into one.
* modify:
  * the working directory name.
* improve:
  * config variables type.

#### Bugfix:
* `AppendEntriesAsyncClient` & `CommitEntriesAsyncClient` aren't properly initialized: their love lived shared_ptr don't share the reference count with the shared_ptr in `DequeNode<T>::DequeNode`, causing very covert problems.
* choose the wrong ownership provider in `Write::EntrustCommitRequest`, fix this by pass the `AppendEntriesAsyncClient*` down through to it.

# 2019-06-21
#### Finish:
* add:
  * `LongLive`: a mechanism to extend the lifetime of the elements popped from `LockFreeDeque` to covering all the time when they are in the outside of `LockFreeDeque`, thus avoid being unexpectedly released while the `LockFreeDeque::GC()` is working.
  * `OwnershipDelegator`: a smart mechanism to avoid the `Write` request object getting released multiple times, and to eliminate the complexity of determining which is the last client who should do the release operation: the obligation is delegated onto the specified delegators.
* improve:
  * some naming conventions.
  * heartbeat checking logic in the follower side shouldn't take the ownership of `FollowerView::m_last_heartbeat_lock` for that much long.
  * make the working threads of grpc migrating from each CQ to get around the `CQ starving` problem.
  * phase state judgement logics in `TwoPhaseCommitContext`.
  * the huge phase statistic and callback control logic in `Write()`(too much to described in detail).
* modify:
  * the configuration list.
  * remove some hard coded timeout settings in leader service unit test.
  * connection pool size is following to the config(a tradeoff, better approach is not figured out).
  * remove unnecessary phase statistic logic in `MemberMgr::Statistic().
  * move some uncommon member functions and variables out from `RPCBase` to the right places.

#### Bugfix:
* client didn't clear up all the context then they are backing to the connection pool: add `ClientBase::ClearCallBackArgs()` to fix.
* the GC problem in `LockFreeDeque` and re-test the whole data structure.
* phase state judgement logics in `TwoPhaseCommitContext`.
* a stupid bug in `ClientPool::GetOneChannel`.

# 2019-06-20
#### Finish:
* post [a bug report](https://gitlab.com/gitlab-org/gitlab-ee/issues/12302) for http://gitlab.com, I love this fantastic version management tool anyway ^_^.

# 2019-06-18
#### Finish:
* modify: 
  * rename `TwoPhaseCommitContext::PhaseState::JudgeOneClusterDetermined()` to a simpler one.
  * a new approach for the CGG problem's solution.
  
* add:
  * add `MemberMgr::Statistic()`.
  * supply the missing code in `MemberMgr::MemberChangeCommitCallBack()`.
  * print more information in `TwoPhaseCommitContext::Dump()`.
  * a correct implementation of `Write::CommitDoneCallBack()` is non-trivial, supply the missing important logics to it.
  * make the statistic and entrusting client stuff in `Write::ReplicateDoneCallBack` right.
  * add `m_last_commit_entrusted`, this phaseII finishing flag.

* refactor:
  * add `TwoPhaseCommitContext::JudgePhaseIPotentiallySucceed()`.
  * extract `UnorderedSingleListNode` out to a common place for multiple usages.
  * make `CommitEntriesAsyncClient::Release()` public for outside usage.
  * add `Write::Finish()`.
  * releasing resources issues in `Write::BeforeReplicate()` and ` Write::AfterDetermined()`.
  * entrusting clients stuff.

#### Bugfix:
* deleting myself operations in `Write` didn't get properly coordinated.
* didn't process some failure cases in `Write::React()`.
* didn't set the follower entity's status before add the `TaskType::RESYNC_LOG` task.


# 2019-06-17
#### Finish:
* improve:
 * modify variable name in ` AppendEntries::BeforeJudgeOrder()`.

#### Bugfix:
* a hierarchy issue: first check [this](https://stackoverflow.com/questions/2157104/c-pointer-multi-inheritance-fun) and then see [this experiment](https://gist.github.com/ppLorins/9134e0c7dfa4f66070a4fcc9b75fc417).

# 2019-06-16
#### Finish:
* remove `LockFreePriotityQueue::TaskType::LOG_REPLICAION`.
* improve: `LockFreePriotityQueue::ThreadEntrance()` using a `std::map` instead of `std::vector` to store the task type list.
* remove the debug feature in `TrivialLockDoubleList<T>::Insert()`.

#### Bugfix:
* `ClientPool::m_channel_pool` didn't get initialized.
* `GlobalEnv::TypePtrCQ GlobalEnv::GetClientCQInstance` index calculation incorrect.
* follower status didn't set to `FollowerStatus::RESYNC_LOG` in `TestLeaderView, GeneralOperation`.
* `StorageGlobal::m_instance.Reset()` didn't re-initialize.
* `StorageMgr::GetSlice()` didn't deal with the case of encountering an empty sstable list.
* `LeaderView::SyncLogAfterLCL()` didn't clear entity list before each sending.

# 2019-06-15
#### Finish:
* refactoring the `memebrship change` and `election` RPCs to the asynchronous version.
* optimizing the client classes hierarchy.
* add some unit test controlling flag.
* code structure optimization in `ElectionMgr::LoadFile()`.
* add the `CQ` template parameter for the class of `UnaryAsyncClient()`.
* update the unit test code to adapt the new refactored asynchronous code.
* add `Reset()` interfaces in `TwoPhaseCommitContext`.
* erase `LeaderView::IncreaseStatistic()`, using internal `Increase*` interfaces inside `PhaseState`.

#### Bugfix:
* didn't use `this->Release()` in `UnaryAsyncClient<T,R,Q>::React()`.
* member variables in `RpcStatistic` didn't get initialized properly.

# 2019-06-13
#### Finish:
* `TestFollowerService` passed.
* `connection_pool_ex*` & `service_ex*` promoted.
* improve previous `connection entry` related implementations on communicating with followers.

# 2019-06-12
#### Finish:
* improve: 
  * make sstable files' name based on both a high precision timestamp and a random number.
  * using smaller data to represent the sstable's meta data.
* add some control flags in the config for unit testing.
* add concurrent test case for sstable.

#### Note:
* the `std::chrono::time_point` returned by `std::chrono::system_clock::now()` has a bad precision at microseconds level. Check [this](https://gist.github.com/ppLorins/cfbbbab9c924009602bafcabcba30352) for details.

#### Bugfix:
* memory table simultaneously dumping issue in `StorageMgr::Set`.
* sstable name conflicting issue.
* storage layer purging process:
  * flushed in memory data into disk.
  * lossing memory table defer delete process.

# 2019-06-11
#### Finish:
* transmit the out of order message process logic in `AppendEntries` to the background tasks.
* improve the implementation of `BinLogGlobal::m_instance.RevertLog()`.
* refactor `AppendEntries::Process()`.
* add `append_entries_start_idx` flag.
* improve `StorageMgr::Initialize()` parsing merged file related logic.

#### Note:
* caveat: you cannot lock the `std::mutex` in thread A, and `wait` the corresponding condition variable object in thread B, causing undefined behavior:

> std::condition_variable::wait_for : If these functions fail to meet the postcondition (lock.owns_lock()==true and lock.mutex() is locked by the calling thread), std::terminate is called. 

  from [here](https://en.cppreference.com/w/cpp/thread/condition_variable/wait_for) and:

> std::mutex::unlock : The mutex must be locked by the current thread of execution, otherwise, the behavior is undefined.
  
  from [here](https://en.cppreference.com/w/cpp/thread/mutex/unlock).

* without signaling a `std::condition_variable` object, there are only two scenarios where it can detect the condition changed :
  * timeout.
  * a spurious wakeup.
  
  That is to say, if using `void wait( std::unique_lock<std::mutex>& lock, Predicate pred )`, you **cannot unblocking it without explicitly signaling it**.
  
#### Bugfix:
* ` Write::React()` finished too early.
* the `JudgeAllFinished` call in ` Write::CommitDoneCallBack` didn't always get executed when it should.
* `StorageMgr::GetSlice()` iterating issue.
* `StorageMgr::DoPurge()` recycle issue.

# 2019-06-10
#### Bugfix:
* `UnaryRequest<T,R,Q>::React` didn't set the `ProcessStage::FINISH` state at a right position.

#### Note:
* calling the server side's [grpc::ServerAsyncResponseWriter<W>::Finish](https://grpc.github.io/grpc/cpp/classgrpc_1_1_server_async_response_writer.html#ad1e22d187c82a537ded4504fbdef6809) will cause a notification on the server side, be careful about the multiple threading problem when dealing with request status.

# 2019-06-09
#### Finish:
* made grpc server gracefully shutdown: shutdown server before shutdown the CQs.

#### Note:
* All completion queue associated with the server (for example, for async serving) must be shutdown after `grpc::ServerInterface::Shutdown()` has returned, see [this](https://grpc.github.io/grpc/cpp/classgrpc_1_1_server_interface.html#ac36477b6a7593a4e4608c7eb712b0d70) and [this](https://grpc.github.io/grpc/cpp/classgrpc__impl_1_1_server_builder.html#adbb6da96e1f9d2d9fee2a0cbc9261023).

#### Bugfix:
* `LockFreeDeque::GC()`  mechanism issues.
* `BinLogGlobal::m_instance.RevertLog` set binlog file status to `BinlogStatus::REVERTING` too early.

# 2019-06-07
#### Finish:
* stopping asynchronous grpc server related logic.
* add the `checking_heartbeat` flag to control whether the follower can start electing or not in unit test.

#### Bugfix:
* `CommonView::m_entity_garbage` didn't get instantiated.
* storage submodule un-initialization incomplete issue.
* `StorageMgr::Initialize()` didn't consider the case of encountering an empty sstable list.
* ` SyncData::Process()` didn't return an status.
* `LeaderView::SyncLogAfterLCL()` didn't always finishing synchronizing logs with a `WritesDone()` and `Finish()`.
* `TestFollowerService::DoSyncData()` didn't read the `::raft::SyncDataMsgType::PREPARE` message's response.
* delete the request instance itself after it has read a false result from the CQ.

# 2019-06-06
#### Finish:
* add sstable file format related descriptions to the `developer_guide.md`.
* finish refactoring the writing process.

#### Bugfix:
* static template member can't be instantiated.

# 2019-06-05
#### Finish:
* refactoring writing process.

# 2019-06-04
#### Note:
* from [this discussion](https://mail.google.com/mail/u/0/#search/raft-dev/FMfcgxwCgzCDDRvgtMgxBltWBQSztgrQ),the raft's leader can't serve reading requests from clients until it has committed at least one entry from its current term? Alright, also making sense.

# 2019-06-03
#### Finish:
* optimize the client classes hierarchy.
* finish new `ClientPool`.

#### Notes:
* `::grpc::ClientContext` instances should not be reused across rpcs, check [this](https://grpc.github.io/grpc/cpp/classgrpc__impl_1_1_client_context.html), [this](https://github.com/grpc/grpc/issues/9427) and [this](https://github.com/grpc/grpc/issues/486).
* no need to maintain a client list in the `LeaderRequest` class, just iterating the ` LeaderView::m_hash_followers` when it's necessary.

#### Todo:
* reuse the `LeaderRequest` class objects.

# 2019-06-01
#### Finish:
* updated [this mail](https://mail.google.com/mail/u/0/#search/grpc/KtbxLxgKJJPkwvGplJvRNNrQlPCVDjKDGV).

# 2019-05-31
#### Finish:
* some refactoring for the `async` version.
* throughput can improve a little after 
   * using multiple channels for each client.
   * using mac as a high performance client pressure issuer.
* find the huge difference on throughput between the `debug` version and `release` version of windows grpc server!

# 2019-05-30
#### Notes:
* `grpc::Channel` is thread safe according to [this](https://github.com/grpc/grpc/issues/5649) and [this](https://www.mail-archive.com/grpc-io@googlegroups.com/msg04317.html).
* sharing a channel among the stubs is valid in C++, check [this so question answered by me](https://stackoverflow.com/questions/47022097/should-i-share-grpc-stubs-or-channels/56375224#56375224).

# 2019-05-28
#### Finish:
* refactoring all the rpc interfaces except for the most complicated one: `Write`.
* found [an awesome tool](https://docs.microsoft.com/en-us/visualstudio/debugger/debug-multiple-processes?view=vs-2015#BKMK_Contents) in visual studio to debug multiple(typically client-server) processes, it's just a killer for me!
* reported [a prospective bug](https://github.com/grpc/grpc/issues/19159).

#### Notes:
  * calling the client side's [grpc::ClientAsyncReaderWriter< W, R >::WritesDone](https://grpc.github.io/grpc/cpp/classgrpc_1_1_client_async_reader_writer.html#af3e090a0801f6bc524f959cd93e4cddc) interface will cause both a notification on the client side as the normal write operations do and a notification on the server side with a `false` result of the CQ.
  
  * calling the client side's [grpc::ClientAsyncReaderWriter< W, R >::Finish](https://grpc.github.io/grpc/cpp/classgrpc_1_1_client_async_reader_writer.html#a292fae35382e0f3c919840689cfb2358) (usually immediately after `WritesDone`) will cause a notification after reading the `final status` value on the client side, but won't cause any notifications on the server side, since this call only intends to let the application know the final status of the rpc to the client.
  
  * calling the server side's [grpc::ServerAsyncReaderWriter< W, R >::Finish](https://grpc.github.io/grpc/cpp/classgrpc_1_1_server_async_reader_writer.html#a75342152acd961b7fcf1317bec0b8c3a) interface will trigger twice notifications immediately on the server side, check [this](https://github.com/grpc/grpc/issues/17222) and [this](https://github.com/grpc/grpc/issues/19159), I suppose their meanings might be:
  
    * First notification: the final status set by the server has already been sent to the wire.
    * Second notification: the stream has already been closed now.
  
    Thus if there are multiple threads polling on the same CQ, a synchronization between them is need.
  

# 2019-05-27
#### Finish:
* refactoring for the `async` version:
  * request classes definitions.
  * main process logic.

# 2019-05-26
#### Finish:
* new branch `async`, start refactoring the aurora server itself.

# 2019-05-24
#### Finish:
* a stream & unary hybrid working example of proxy server.
* grpc new version v1.21.x helps nothing on the throughput on windows.

# 2019-05-23
#### Finish:
* The impact on throughput of `#channel` and `#stream`, read [this](https://groups.google.com/forum/#!topic/grpc-io/zvwLNUC8PWQ).
* [an good online debug tool](https://grpc.io/blog/a_short_introduction_to_channelz/) for live grpc service.
* managed to work out an async stream proxy example, non-trivial evolvement.

# 2019-05-22
#### Finish:
* Update grpc on windows to `v1.21.x` and its dependency of `boringssl` & `protobuf` to the lastest. Disgusted me a lot.
* Finally found a working sample of async c++ stream client & server, inspired by [this](https://groups.google.com/forum/#!topic/grpc-io/AlwaSuDTcoM). Note another helpful [mail](https://groups.google.com/forum/#!topic/grpc-io/pGx33aLmaCg).

 > 3. The server side doesn't ever call `WritesDone`; that is a client-side thing. The server-side only calls Finish.

# 2019-05-21
#### Finish:
* an solution for the `AsyncNotifyWhenDone notification` problem: build a map between the `calldata instances`' ID and its notifications' ID.
* [grpc transport explanation](https://github.com/grpc/grpc/blob/master/doc/core/transport_explainer.md).
* Found I'm not the only one who are facing the poor throughput on windows, check [this](https://github.com/grpc/grpc/issues/14378). 

#### Todo:
* right way to do the async stream on the server side.
* update grpc to the lastest to retry the benchmark.

# 2019-05-20
#### Finish:
* building an example of a Mixed version of bidirectional stream & unary communicating between grpc server and grpc client.

# 2019-05-09 ~ 2019-05-19
Being lazy at work, and did't note for much. Just some recaps:
* multi-paxos investigation, and [a good blog series](http://oceanbase.org.cn/?p=111) to be noted.
* learned the [grpc polling engine](https://github.com/grpc/grpc/blob/master/doc/core/epoll-polling-engine.md).
* Successfully build an example for bidirectional stream communicating between grpc server and grpc client.
* Trying to figure out why the async c++ server of grpc performs poorly on throughput:
  * build a released verion of the grpc server.
  * scale `#CQ` & `#thread` & `#pool-size`.
  
  None of these worked. Confusing now, details are [here](https://mail.google.com/mail/u/0/#search/grpc/KtbxLxgKJJPkwvGplJvRNNrQlPCVDjKDGV).

# 2019-04-26 ~ 2019-05-08
Back home & on vacation.

# 2019-04-25
#### Finish:
* document basically finished.

# 2019-04-24
#### Finish:
* master how to write documents in the `LaTex` form, that's an awesome expressing format.
* document evolving.

# 2019-04-22
#### Finish:
* document evolving.
* learned about the [write amplification](https://en.wikipedia.org/wiki/Write_amplification) problem related stuff.

# 2019-04-21
#### Finish:
* document evolving.
* learned about the `zab` which is quite similar to `raft` in many ways. Some key points:
  * the `zab` cluster has three status:
    * discovering : there is no leader at present, need to find one.
    * syncing : sync data to **all of the nodes**.
    * ready : cluster is ready to serve.
  * the fast leader election (FLE) important properties :
    * nodes only increasing(by 1) their `logicCount` when a leader gone event detected.
    * `logicCount` must be the same among each node when voting.
    * when updating its voting, the node also broadcasting this update to the cluster.
    * when a node find it has got the majority:
      * if all node have voted in its local record, it turns to the leader.
      * if not all node have voted in its local record, it waits for about 200ms to receive the remain votes.

#### Todo:
* improve:
  * consider possible `mmap` based optimizations, e.g., SSTable accessing.

# 2019-04-20
#### Finish:
* document evolving.

#### Todo:
* occationally appearing bug in `TestStorage.ConcurrentOperation` : 
*test_storage.h(83): error: Value of: _val == _val_2
  Actual: false
Expected: true
_key:key_471,expect _val:val_471,actual _val:val_615*

* performance issue of `StorageMgr`.


# 2019-04-19
#### Finish:
* simplify `GuidGenerator`. There is no need to persist last released ID, the `ID-LRL` is 
  all that needed.
* document evolving.

# 2019-04-18
#### Finish:
* improve: use recursive lock instead of `mutex` for `TrivialLockDoubleList::CutHead`.
* document evolving.

# 2019-04-16
#### Finish:
* understand how the async version of `grpc server` is working.It's time to refactor the sync version
  to the async version,some notes:
  * [multi thread scaling](https://github.com/grpc/grpc/issues/9728#issuecomment-280417384).
  * [a helpful mail](https://groups.google.com/forum/#!topic/grpc-io/DuBDpK96B14).
* [another great doc](https://github.com/grpc/grpc/blob/master/doc/core/epoll-polling-engine.md) helping
  understand grpc's polling engine.

* multiple thread can blocking on the same epoll fd's `epoll_wait` call:
> If multiple threads (or processes, if child processes have inherited
       the epoll file descriptor across fork(2)) are blocked in
       epoll_wait(2) waiting on the same  epoll file descriptor and
       a file descriptor in the interest list that is marked for edge-
       triggered (EPOLLET) notification becomes ready, just one of the
       threads (or processes) is awoken from epoll_wait(2).  This provides a
       useful optimization for avoiding "thundering herd" wake-ups in some
       scenarios.

   see  [this](http://man7.org/linux/man-pages/man7/epoll.7.html).

* document evolving.


# 2019-04-14
#### Finish:
* found it's hard to master cmake in a short time , it's not friendly to new users.Thus downgrade its priority
  to the lowest among the remaning works:
  * document building. (feature.)
  * defer free solutions for `LockFreeDeque::Pop()`. (bug.)
  * `AsyncNext` stuck issue. (bug.)
  * improve overrall throughput. (performance.)
  * sstable meta data minimization. (improment.)
  * cross-platform compliation. (improment.)

# 2019-04-13
#### Finish:
* compiles on clang-1000.10.44.4 under x86_64 darwin.

# 2019-04-12
#### Finish:
* a temporary solution for memory leak and security issue for `LockFreeDeque`.
* visio works of `file_format.vsdx` and `file_format` .
* licese related works.

# 2019-04-11
#### Finish:
* refactor `m_entity_garbage` in `LeaderView` and `FollowerView`.
* add `LockFreeDeque::PopNode`.
* visio works of `lock_free_deque.vsdx` and `lock_free_hash.vsdx` and `lock_free_queue.vsdx`.

# 2019-04-09
#### Finish:
* take an investigation of `graphviz`,found it's hard to adjust node's position,give it up.
* visio works of `trivial_lock_list.vsdx`.

# 2019-04-08
#### Finish:
* many features during the last week.Can't write details. Now this project is ready to launch beta.

# 2019-03-31
#### Todo:
* consider a possible optimized solution of using `CompletionQueue` : on the leader side, all requests
  are send and processed using one `CompletionQueue` .

# 2019-03-30
#### Finish:
* learned a method to deal with `ABA probelm` in `CAS` : `tagged pointer` ,which relies on the fact that 
  the x86_64 architecture leave the 16 most significant bit of a memory address empty,which in turn can be
  exploited to represent the verions of that address.
* some kernel bypass technologies : [](https://blog.cloudflare.com/kernel-bypass/).

# 2019-03-29
#### Finish:
* upgrade `LockFreeHash` : from unary hash to binary hash , like `std::unordered_map`.

# 2019-03-28
 Each time when I was coding in the night and listening the song of *川井憲次 (かわい けんじ) - 孤独な巡礼*  ,I 
couldn't help stopping ,leaving hands from the keyboard, truning to just enjoy the slience around me,it can 
never be so friendly till that music comes, feeling like water slowly flowing through my body,  warm and peaceful.

#### Finish:
* renaming lock-free related data structures.
* part of `LockFreeHash` modification : from unary hash to binary hash.

#### Bugfix:
* `HashNode<T>::operator==`

# 2019-03-27
#### Finish:
* Found a bug in grpc design ,details [here](https://github.com/grpc/grpc/issues/18472#issuecomment-477269574).
  This bug seems not easy to get around , got no choice but hanging it for now.
  * After several tests,I'm quite sure that it is not the receiver side's bug , it should be caused 
     by the `AsyncNext()` and `Next()`'s design.
* add some debug code and improve some unit test.

# 2019-03-26
#### Finish:
* refactor some unit tests.
* remove debug logs.
* located the performance bottleneck of follower service: the grpc threading model.

# 2019-03-25
#### Finish:
* after 3 days of debuging and upgraded 2 versions ,a final single-linked-list version of MPMPC came out.

# 2019-03-24
#### Finish:
* accidently found a solution for MPMC queue to enhance throughput and it's very close to wait-free.

# 2019-03-22
#### Finish:
* now the three part of raft:
  * log replication.
  * election.
  * membership change.

  are function complete,further work including bugfix and performance improment.

#### Note:
* post a new [bug report](https://github.com/grpc/grpc/issues/18472) to the grpc community.

#### Bugfix:
* use `ElectionMgr::m_chagned_term` to replace the useless `ElectionMgr::m_term_changed` flag.
* `_pre_term` in `RaftServiceImpl::Write` may be different from current term after a new leader 
  just came out.

# 2019-03-21
#### I've got so much things done today!
#### Finish:
* add:
  * `heartbeat_oneshot` config option control leader's heartbeat behaviour more precisely 
    in unit test.
  * persist `ElectionMgr::m_known_voting`  to config file.
  * unit test of `TestMember::Election`.
  * `TestMultipleBackendFollower::StartOneFollower` support different config files copy
     for empty followers .
  * `Dump` methods for the background tasks.
  * leader gone away related logic.
  * `RaftServiceImpl::ValidClusterNode`.
  * `ServerStatus::SHUTTING_DOWN`.
  * `StateMgr::m_nic_addrs` and its related (possibly useful) logics.

* modify:
  * `JointTopology::Update` support self-updating.

* improve:
  * some naming convetions.
  * what if binlog don't have enough entries before `ID-LCL`? Here just simply return false.

* refactor:
  * maked `ElectionMgr::CandidateRoutine` less nesting.
  * `GlobalEnv` do not use `using xxxx;` statement.
  * reduce nesting levels in `RaftServiceImpl::PrepareReplicationContext`.

#### Bugfix:
* `BinLogGlobal::m_instance.m_p_meta` concurrently accessing problems.
* consistent reading problems for `ElectionMgr` and `LogReplicationContext`.
  * solved by adding a snapshot variable of `JointSummary`.
* election voting counter logic.
* leader didn't propogate heartbeat messages when in joint consensus state.
* `m_joint_summary.m_joint_topology` didn't get fully updated after reading from config file.
* `m_last_trigger_guid` didn't synchoronized under multiple thread accessing .
* `GlobalEnv::ShutDown()` shouldn't be executed inside a grpc thread.

# 2019-03-20
#### Finish:
* improve:
  * remove the `m_conn_todo_set.erase` logic,it can't do what it wants and is unnecessary.
  * `Dump()` implementations in background task.
* unit test of `TestMember.WriteData`.

#### Bugfix:
* bit operator wrong usage in `LeaderView::IncreaseStatistic`.
* statistic bugs.

# 2019-03-19
#### Finish:
* add:
 * `MemberMgr::WaitForSyncDataDone`.
 * `GlobalEnv::IsRunning`.
 *  `TestMember` support configure leader gone flag.
* improve:
  * `TestMember` & `TestCluster`.
  * using `PhaseState::m_cq` to do asynchronous thins rather than a temporary local variable.

#### Bugfix:
* a stupid bug that a reference cannot be re-assign since it will cover the old data with the new one!

# 2019-03-18
#### Finish:
* refactor:
  * variable naming.
* improve:
  * some loggings with more detailed descriptions.
* modify:
  * `MemberMgr::PropagateMemberChange` .
  * wailt until all nodes get synced before propagating membership change rpcs.
  * use `cq.Next` instead of `cq.AsyncNext` to get around problems:
    * rpc is sent out before 'AsyncNext' is called (done,not guaranteed,see [here](https://github.com/grpc/grpc/issues/14104)).
    * rpc is not sent after 'AsyncNext' is called (hard to reproduce,but saw 2-3 cases).  

#### Bugfix:
*  missing some fields initialization and uninitialization in `ConnPoolEntry` & `JointTopology`.

#### Note:
* found that there is problem in my current implementation for `membership change`: if some new nodes
  sync the old data(data & log in the leader) fail, there will be changes for them to recover in the 
  later time, whereas the paper's implementation will not(it's a log entry and will be resent if leader 
  found some new nodes lag behind). For this reason, in the starting time of the membership changing process, 
  the leader wait until **ALL** the new nodese get synchronized.

# 2019-03-17
#### Finish:
* modify: 
  * the interacting form in the stream rpc of `SyncData`. Previous confirmation mechanism proved
     to be unnecessary ,but just introducing complication.
  * remove `LogReplicationContext::RpcState`.

* add:
  * `BackGroundTask::TwoPhaseCommitContext`.
  * `MemberMgr::m_in_processing`.

* refactor: 
  * extract `BackGroundTask` out of `leader_view.h/.cc`.

#### Bugfix:
*  `MemberChangeCommit` rpc shouldn't be call on every node.

# 2019-03-16
#### Finish:
* improve : 
  * remove redundant assign statement 
  * remove some debug logs.
  * naming .
  * timeout values for `AsyncNext` in `MemberMgr::PropagateMemberChange`.
* add : `GlobalErrorCode`.

#### Bugfix:
* `SyncDataContenxt::m_id_lcl` ,it shouldn't be exist.
* In `LeaderView::SyncDataCB` , when `list.empty()` satisfied,shouldn't return.
* `JointTopology` update issue in `MemberMgr::PropagateMemberChange`.
* remove `RaftRole::EMPTY`,it's proved not necessary but just introducing problems.

# 2019-03-15
#### Bugfix:
* `LockFreePriotityQueue::TaskType` enum value definition order issue.
* `ReSyncLogContext::m_last_sync_point` didn't get updated before each new round of resyn.
* `MemberChangeInnerRequest` missing required field .

# 2019-03-14
#### Finish:
* refacotr:
  * `MemberMgr` refactored a lot. It's more clear as a whole.
  * `TestMultipleBackendFollower::StartFollowers*` .
* modify:
  * some naming conventions.
* add:
  * heartbeat flag when doing unit test,toggle whether sending the heartbeat message or not.
  * `RaftRole::EMPTY` state indentifying a newly about to be joined node and it's related logics.
  * `TestMember` in unit test.
#### Bugfix:
* statistic issue in `RaftServiceImpl::PrepareReplicationContext`.

# 2019-03-13
#### Finish:
* refactor : some naming conventions.
* improve :
  * timeout mechanism in `AsyncNext`.
  * timeout value in `*pending_list_timeo`.
  * `LeaderView::StartPhaseII`.
  * remove `CommonLogic` in `LeaderView` to simplify things.
* modify :
  * using bit mask rather than int value to represent the old-new cluster state.
* add :
  * term changed falst to indentify a term change for `Write`.
  * support heatbeat timeo value when starting child processes in unit test..
#### Bugfix:
* new leader's previous term issue.
* entrust number issue.
* unit test cluster start followers isssue.

#### Todo:
* why it cost so much time between each round of `AsyncNext` processing.
* server stuck issue(this time caseud follower heatbeat timing out and unexpectly switched to candidate) 
  already found when did benchmark test before.

# 2019-03-12
#### Finish:
* add some logic in unit test.
* improve :
  * `GuidGenerator`: make sure `_release_start >= last_released` always satisfied in the
    begining of generating new guids.
  * remove redundant `SyncDataRequest` in `SyncDataContenxt`.
  * naming conventions for some macros and long variable names.

#### Bugfix:
  * some unit issues.


# 2019-03-11
#### Finish:
* remove the `PhaseStatem::_unclear_fail_num` field, it's too ambiguous.
* apply joint consensus roles to log replication .
* finish membership change in both `log replication` and `election`.This is a milestone.
* improve `JointSummary::m_added_nodes` : do not initializing a whole follower entity 
  object(contain connection pool overhead) when it's in a follower server.

#### Bugfix:
* follower's checking heartbeat timeout task in the global timer should quit after it
  detecting a timeout occured and launched the election thread.


# 2019-03-10
#### Finish:
* make `LockFreePriotityQueue::TaskType` and `Leader::FollowerStatus` simpler.
* refactor `RaftServiceImpl::Write`.
* add `JointConsensusMask` to `FollowerEntity` and its related logic.
* update followers info in the leader side when joint consensus happened.
* shutdown server when it's not in the new cluster.

#### Todo:
* bugfix of storage didn't persist all log entities in `_input_list` which is in 
 `RaftServiceImpl::Write`.

# 2019-03-09
#### Finish:
* add `version` mechanism for membership changing.
* remove redandunt message definitions in `raft.proto`.

#### Bugfix:
* guid should be re-initialized every time switching roles.

# 2019-03-08
#### Finish:
* all membership change logics except for the part of how to follow membership change rules when 
  making agreement decisions(elections and entry commitment).

#### Note:
* In my implementation , membership changing is separated from log replication.That is , 
  the transitions between `C-old` to `C-old-new` , and `C-old-new` to `C-new`
  are not recorded in server's binlog, but they are communicated by independent RPCs.
  I hate insert logic into operating on binlog files , I like it to be as pure as possible. As a 
  consequence, the election process should add an additional logic: a candidate requesting for vote 
  not just based on its logID, but also the `joint consensus status`  which is that if a candidate 
  found a requester's logID is equal to mine, it should also having a fresher `joint consensus status`
  than mine Which may sounds confusing , but treat it like a log entry which should have been appended
  in a log , but now it is carried by a separated field in the voting RPC.


# 2019-03-07
#### Finish:
* improve `LoadFile()`.
* macro naming convention improvement.
* code indention improvement.
* validity checking improvement.
* add `RaftCore::Member::MemberMgr` and some RPCs related to membership change.

#### Todo:
* `RaftServiceImpl::Write` & `RaftServiceImpl::AppendEntries` are too long,refactoring.

# 2019-03-05
#### Finish:
* refactor of `RaftCore::Leader::FollowerStatus`.
* sync data part of membership change.

# 2019-03-04 - partII
#### Finish:
* improve : `LeaderView::SyncLogAfterLCL`.
* check the requesting node address's validity in `PreVote` `Vote` `HeartBeat` RPCs.
* `LeaderView::ReSyncLogCB` support continuous syncing when met `ErrorCode::WAITING_TIMEOUT` error.
* `RaftServiceImpl::MembershipChange` is done.
* some of the catch up logic in the start phase of membership change.
* code refactor of `::raft::ErrorCode`.

# 2019-03-04
#### Finish:
* bugfix:
  * `jumping term` issues.
  * `topology` update issues when switching role.
* improve:
  * `ElectionMgr::AddVotingTerm` also collecting infomations in `pre-vote` stage.
  * replace `std::list` with `std::set` to represent topology in memory view.
* election unit test is basically succeed now.

# 2019-03-03
#### Finish:
* bugfix:
  * majority number calculation problem.
  * logging's wrong indication of `pre-vote`  and `vote`.
  * term didn't change when switching role. 
  * remove the **dumb follower** status.
* some logging improments.
* unit test :
  * election.
  * support more flexible way of starting the background follower processes.

#### Note:
* In my previous implementation , there is a **dumb follower**  state means that when a node falls into 
  a follower state,either from the *leader state* or the *candidate state*, it will not start its
  checking mechanism , until receiving a heartbeat message from leader.
* There is a live lock problem in election that the raft paper didn't mention : the nodes with stale 
  logs could increasing their terms always faster than the nodes with fresh logs. The bigger sleeping 
  gap between each round of elections , the more likely  this could happened. This can be significantly 
  mitigated by using a `jumping term` policy. Details will be explained in other docs.

# 2019-03-02
#### Finish:
* improve : move the `Initialization` and `UnInitialization` method of Class `GlobalTimer` and `ElectionMgr`
  from `CommonView` to `GlobalEnv` for accurate control .
* typo correction.
* bugfix : 
  * missing requried fields of pb message when do heartbeating in `ClientPool`.
  * the `event_counter` issue in `AsyncNext` .
  * timeout issue of`AsyncNext` .
  * `GlobalEnv::UnInitialEnv` issue of reading wrong role .
* refactor:
  * `ClientPool::HeartBeat()` method add input parameters.
  * add `ElectionMgr::WaitElectionThread` method .
  * add `GlobalTimer::ETimerThreadState` to make things about thread exiting clear.
* test case `TestElection.GeneralOperation` is passed.


# 2019-03-01
#### Finish:
* use a global lock `ElectionMgr::m_election_mutex` to make election process simpler.
* find the minimux term that is never used by the cluster when candidate start the 
  next round of election.
* resolve compile warnings.
* code refactoring.

# 2019-02-28
#### Finish:
* bugfix in `GlobalEnv`.
* some of the election logic.
* some code refactor.

#### Todo:
* figure out why `LockFreeDeque::Push`  and `LockFreeDeque::Pop` cannot execute simultanously.
* figure out why the time in milliseconds between leader sending the heartbeat request and the follower
  receive it is so much : approximately 7ms ~ 8ms, thread scheduling policy ?


# 2019-02-27
#### Finish:
* found another [bug](https://developercommunity.visualstudio.com/content/problem/155480/vc-cannot-use-a-const-char-as-non-type-template-ar.html) 
  in `msvc 2015` , I had to get around it by modifying the implementation of `ElectionMgr::BroadcastVoting`, 
  `MSVC2015` and even `MSVC2017` are extremely unfriendly to some c++11 features.
* improve `timer` : avoiding one task caused all tasks infinitely timing out.


# 2019-02-25
#### Finish:
* support switching role globally.
* refactor : `include` improve convention.

# 2019-02-23
#### Finish:
* `CommonView` refactor.
* add `ElectionMgr`.
* add `PreVote` and `Vote` grpc interface.

# 2019-02-22
#### Finish:
* refactor & modify : 
  * raft grpc request definitions.
  * validity check logic.
* candidate related logic in `StateMgr` and `CTopologyMgr`.

#### Todo:
* find a cross-platform way to get local ip address.`boost:asio` working poorly under `darwin`.

# 2019-02-21
#### Finish:
* add `TrivialLockDoubleList<T>::DeleteAll` method avoid using `TrivialLockDoubleList<T>::Clear`
  will confilict with `TrivialLockDoubleList<T>::Insert`.
* code refactoring:
  * `using` keyword usage conventions.
* bugfix : 
  * In `RaftServiceImpl::GetConservativeTimeoutValue`, the non last released guid should 
    have waitted for a longer time than the last released guid.
* modifying `StateMgr` and `GlobalEnv` & adding `CandidateView` to get prepared for election logic.


# 2019-02-20
#### Finish:
* decide to jump over `cmake`,that is a time consuming job.

#### Todo:
* the performance difference between `darwin` and `win10` may be caused by the `hyperV` feature,
  try to close it and re-run the unit test to confirm.

# 2019-02-18
#### Finish:
* found & get rid of a boost 1_68 bug in `boost::filesystem::copy`, 
  reported [here](https://github.com/boostorg/filesystem/issues/104).
* modify background process startup command to adapte `OS X`.
* make `VLOG` configurable for child processes.
* cross platform compiling is basically done except for joining them together with `CMake`.

# 2019-02-17
#### Finish:
* able to start a follower process under `darwin`.
* add methods to allow grpc service to stop.
* bugfix :
  *  offset didn't go back when rotating file.
  * `FollowerView::UnInitialize()`  didn't call `CommonView::UnInitialize`.

# 2019-02-16
#### Finish:
* bugfix :
  * there is a trailing `\r`  when using `std::getline()` under `darwin` which will causing 
    its subsequent `RE` parsing procedure fail.
  * `LockFreeHash<T>::Clear()`  issue which is missed getting tested before.
  * `DoubleListNode::m_deleted`  isn't initialized, causing occasionally issues under `darwin`,
     this mistake shitting me a lot,take a lot time to debug it, go nuts!
  * `TestTrivialLockPriorityQueue.ConcurrentOperation`  test case didn't use `LockFreePriotityQueue::UnInitialize()` 
     to quit, causing issues under `darwin` which threads executing is much faster.

#### Note:
* found unit tests under my `darwin`(8 cores with 2.2GHZ each) are generally 30x faster than undering 
  my `win10` (8 cores with 4.0GHZ each), I can't figure out why , guessing may the schedule policies 
  be the cause.

# 2019-02-15
#### Finish:
* fixed up my mac environment problems:
  * make tmux can access system clipborad.
  * make vim can access system clipborad.
  * upgrade several `brew installed packages`.
  * delete several infrastructures from `brew installed packages`.

* compiling project under darwin succeed !

#### Note:
 * [a valuable answer](https://stackoverflow.com/questions/10726321/how-to-ensure-a-target-is-run-before-all-the-other-build-rules-in-a-makefile) about how to ensure some commands will be executed before any of the makefile targets start
   evaluating

# 2019-02-13
#### Finish:
* test some Makefile features .
* finish `aurora` example makefile.

#### bugfix:
* some syntax issues that only reported under `OSX`.

# 2019-02-12
#### Finish:
* compile & run `glog` `gflags` `boost` demo under `OSX`.
* learning some basic cmake knowledge.

# 2019-02-02 to 2019-02-11
#### Finish:
* This is during the Spring Festival(Happy Pig's Year ^_\^ ).
* upgrade to high sierra^10.13.6^ and fix several environment issues(`brew` `xcode`) on my macbook.
* delete several forks on [my github](https://github.com/ppLorins) and thus on my `OSX` ,pointing to 
  the original upstream addresses.

# 2019-02-01
#### Finish:
* found a shit for `VS2015 (Microsoft (R) C/C++ Optimizing Compiler Version 19.00.24215.1 for x86)`
  details are [here](https://gist.github.com/ppLorins/09de033a4b0748d883c8bf8fe12b7703).
* fix bugs in `TestLeaderView`.
* fix bugs in GC logic.
* code refactor & bugfix for [ `GlobalEnv` and `FollowerView` and `LockFreePriotityQueue`
   adn `TrivialLockDoubleList` and `` ].

#### Todo:
* program stall in pressure test.

# 2019-01-31
#### Finish:
* close some test flags.
* code refactor of `TrivialLockDoubleList`.
* add global timer to schedule periodic jobs(except MCMP queue consuming) uniformly.
* defer `TrivialLockDoubleList::ReleaseCutHead` to the background GC thread.
* solve gtest and gflag conflict issue.

# 2019-01-30
#### Finish:
* bugfix of mutex usage in `RaftServiceImpl::Write` and `FollowerService::AppendEntries`.

#### Todo:
* solve the majority logic conflict with connection pool reclaim problem. 
* use the `spin lock` instead of `mutex` in `TrivialLockDoubleList<T>::CutHead` to enhence performance.

# 2019-01-29
#### Todo:
* ~~`TrivialLockDoubleList::CutHead` return a `std::shared_ptr` instead of raw pointers.~~(
  * not a proper method to solve the `cut off list releasing & inserting conflict issue`)

# 2019-01-28
#### Finish:
* fix memory consumption too much issue.
* `BinLogGlobal::m_instance.AppendEntry` mutex lock issue.
* some code refactor.

# 2019-01-27
#### Finish:
* bugfix of `TrivialLockDoubleList`.

# 2019-01-26
#### Finish:
* go to HK and bought a luxury bag for my wife as the Chinese new year present.


# 2019-01-25
#### Finish:
* fix several bugs under concurrent execution environment.

#### Note:
* `AsyncNext`  strange behavior still not resolved:
   timeout value set for  it didn't take effect when the backend peer didn't respond. 
     Expected behavior is either to issue a timeout error for the RPC itself in the 
    `::grpc::Status` object or to issue a timeout error for the `AsyncNext`. But neither
     of them happened.  The 'AsyncNext' call just block regardless of any of the timeout values.

#### Todo:
* memory leak issue.
* ~~`TrivialLockDoubleList`  CAS fail issue in cuthead.~~
* `TrivialLockDoubleList`  cut off list releasing & inserting conflict issue.
*  ~~fixbug : `AsyncNext` timeout after a long run .~~

# 2019-01-22
#### Finish:
* fix the usage bug of `LockFreePriotityQueue`  in `LeaderView`.
* test & bugfix of the general operation of `LockFreeQueue`.

#### Todo:
* why 10020 & 10021  lag behind a little.
* why 10022 always lag behind a lot and run out of available connections.

# 2019-01-21
#### Finish:
* test & bugfix of the concurrent operation of `LeaderView`.
* bugfix & improve of `TrivialLockDoubleList`.
* test & bugfix of the general operation of `LeaderService`.
* some test & bugfix of the concurrent operation of `LeaderService`.

#### Todo:
* between calling `LeaderView::CommonLogic::ParsePhaseICQ` and `LeaderView::LogReplicationCB`
  there will be one connection to each undetermined follower hold by the leader,resulting a 
  potential connection running out danger.Can be mitimized by increasing the connection number in
  the connection pool, but how to get rid of this?
* an unproduced issue: in the `TestLeaderView::ConcurrentOperation` unit test,the follower *10020*
  reported a `cut head empty` check error.

# 2019-01-19
#### Finish:
* test & bugfix the general operation of `TrivialLockDoubleList`.

# 2019-01-16
#### Finish:
* bugfix in  `TrivialLockDoubleList` again.

#### Todo:
* Generate a `ReSyncLogContext` only after the majority has a deterministic result.

# 2019-01-15
#### Finish:
* bugfixs in  `TrivialLockDoubleList`.
* bugfixs in  `LeaderView::LogReplicationCB` and `LeaderView::StartPhaseII`.
* some code refactoring and enhance comments.

#### Todo:
* `Concurrent Delete` issues in `TrivialLockDoubleList` : the output list may still contain deleted 
  elements even though `SiftOutDeleted` already there.

#### Note:
* about `grpc::CompletionQueue::Next()`: it only return false  after `drained` and `shutdown`.
* using `std::move` to return a rvalue reference in the `return statement` of functions 
  is not recommended since it can hardly bring performance benifits.

# 2019-01-06
#### Finish:
* some bugfix of `LeaderView`.

# 2019-01-05
#### Finish:
* setting up testing environment for `LeaderView`.
* some bugfix in `RaftServiceImpl::Write`.
* code refactor for testing structure.

# 2019-01-02
#### Note:
* About `linux named pipe`:
  * multiple readers will get message in a *superficially one by one manner*.
  * *read*(*write*) operations will get blocked until the other side *writing*(*reading*)
   something *to*(*from*) it.

# 2019-01-01
#### Finish:
* testing & bugfix of `ClientPool` & `FollowerEntity`.
* using `RE` and support comment(`#`) of topology config file.

# 2018-12-31
#### Finish:
* testing & bugfix of `FollowerService` concurrent operations.

# 2018-12-29
#### Finish:
* bugfix & improve of rotating file logic.

#### Todo:
* ~~check why it cost so much time to allocate a `FileMetaData` object.~~(It has 500k+ elements in
  its `LockFreeHash` member, reasonable.)

# 2018-12-28
#### Finish:
* bugfix of binlog parsing issue.

#### Note:
* Open mode of `std::fopen` function should **ALWAYS** with **BINARY MODE**, otherwise encoding
  characters will be automatically aded to the file content violating `protobuf` format.

# 2018-12-27
#### Finish:
* testing & bugfix `FollowerService` general operations.

#### Todo:
* test concurrent operations of `BinLogGlobal::m_instance.SetHead`.
* ~~binlog parsing issue in `FollowerService` general operations.~~
* ~~test the `rotate file` operation.~~

# 2018-12-26
#### Finish:
* some of the previous *todo list* .

#### Note:
* about `stream` operations synopsis:
    * server get into the stream interface when `ClientReaderWriter` object is constructed at
      cient side.
    * `WritesDone()` make `stream->Read` return false thus break its reading loop.

# 2018-12-25
#### Finish:
* wrote some testing code & bugfix of `FollowerService::SyncData` .
* add new success return code of the `AppendEntries` rpc interface.

#### Todo:
* ~~clear about the correct usage of `m_strea->WritesDone()` .~~
* ~~customize `LockFreeQueue` initializing size process.~~
* consider possible optimizations to reduce `BackGroundTask` memory overhead especially
  in `LockFreeQueue`.


# 2018-12-24
#### Finish:
* wrote some testing code of `FollowerService::SyncData` .
* some bugfix of binlog and `FollowerService::SyncData`  service.

# 2018-12-21
#### Finish:
* testing & bugfix `FollowerService::AppendEntries` & `FollowerService::CommitEntries`.

# 2018-12-20
#### Finish:
* bugfix of `TrivialLockDoubleList::CutHead` and `BinLogGlobal::m_instance.AppendEntry`.
* almost finish testing & bugfix `FollowerService::AppendEntries`.

# 2018-12-19
#### Finish:
* bugfix in `main.cc`.
* modify & test server launching framework.
* add & test service testing framework .

# 2018-12-18
#### Finish:
* testing & bugfix `BinLogOperator` cocurrent operations for `AppendEntry` and `RevertLog`.
* optimize : comparators of `::raft::EntityID` and `LogIdentifier`.
* finish some items of previous *TODO list*.

#### Note:
* for the `std::condition_variable::wait_for(lock,rel_time,pred)` function : threads
  that are block on the call won't unblock as long as pred is unsatisfied  even the `cv`
  variable is signaled by soem other threads . the logic is here:
  ![cv::wait_for](https://c1.staticflickr.com/5/4808/46364387491_2f42ab1752_o.png)

# 2018-12-17
#### Finish:
* code refactoring ,especially for long name types.
* testing & bugfix `BinLogOperator` except for cocurrent `AppendEntry` and `RevertLog`.

# 2018-12-16
#### Finish:
* testing & bugfix general operations of `BinLogOperator` .

#### Note:
* A pitfall : For calculating a correct crc32 value , a `boost::crc_32_type` object can't be used 
 multiple times. Instead it must be declared each time using it.

# 2018-12-15
#### Finish:
* testing & bugfix of `FileMetaData` .

# 2018-12-13
#### Finish:
* testing & bugfix of `common` .
* some testing & bugfix of `FileMetaData` .

# 2018-12-12
#### Finish:
* testing & bugfix of `GuidGenerator` .

# 2018-12-11
#### Finish:
* some testing & bugfix of `GuidGenerator` .

#### Note:
* Unexpectly found that `std::CAS` operations are implemented by spinlock:
   ![spinlock](https://c1.staticflickr.com/5/4847/45551882364_ebd2509661_o.png)

#### Todo:
* fix bug:
  * ~~`LeaderView::m_entity_list` no delete operations.~~
  * ~~There is no guarantee of `LeaderView::m_entity_list::CutHead`'s 
    execution sequence .~~

* check:
  * ~~check why didn't find issues when testing `TrivialLockDoubleList` even `Delete` & `CutHead` 
    are not intended to be invoked simultaneously.~~(Yes, it CHECKED OUT)

  * check all possible `ABA problems` where CAS operations are taken place.

* Optimization:
  * consider a possible optimize for `compare_exchange_strong` : using `compare_exchange_weak` and
    check whethe the `expected old value`  value changed to distinguish `real fail`(changed) from
    `spurious fail`(unchanged).
  
  * consider possible `memory order` optimizations for CAS operations.

# 2018-12-10
#### Finish:
* testing & bugfix of `topology` is done.
* testing & bugfix of `storage` is done.
* testing & bugfix of `state` is done.

# 2018-12-09
#### Finish:
* testing & bugfix of `LockFreeQueue` is done.
* testing & bugfix of `LockFreePriotityQueue` is done.
* testing & bugfix of `utilities.h` is done.
* code directory structure refactoring.

# 2018-12-07
#### Finish:
* testing `TrivialLockDoubleList` is done.
* some testing & bugfix of `LockFreeQueue` .

# 2018-12-05
#### Finish:
* some testing & bugfix of `TrivialLockDoubleList` .

# 2018-12-04
#### Finish:
* testing & bugfix of  `TrivialLockDoubleList` general operations.

# 2018-11-30
#### Finish:
* some testing logic for `TrivialLockDoubleList`

# 2018-11-29
#### Finish:
* modify `LockFreeHash` implentation to not taking ownership when inserting elements.
* finish testing `LockFreeHash`, fix bugs.
* improve gtesting framework.
* refactoring:
  * add a new seperated state directory for the related source files.

# 2018-11-28
#### Finish:
* gtesting with a great start : basicly no errors found.
* a gtest coding skeleton is figured out.

# 2018-11-26
#### Finish:
* CGG problem solution.
* follower left behind forever problem.

#### Todo:
* how to use grpc's authentication mechanism in cpp.
* possible feasible improvements:
 * for the latest log , consider not waiting for a fixed duration ,instead using a *condition variable*
   to do notification between the latest log and its previous logs(kinda pedantic).

# 2018-11-25
#### Finish:
* found a `std::chrono::nanoseconds` issue , details are [here](https://gist.github.com/ppLorins/c16331a8119504b491077158ed26957c).
* some of the CGG problem.

#### Note:
* accidentally found a [discussion](https://mail.google.com/mail/u/0/#inbox/FMfcgxvzKklQMPbztxhZmHZjxvMVGqrT),lucky to
  implement the *SYNC-DATA* process in the current manner, working around that problem which is 
  the c++ client can't detect server's abnormal status during the stream writting and most 
  likely is a bug.
 

#### Todo:
* ~~code refactoring: do not use the class prefix when accessing *static* members inside that class.~~

# 2018-11-24
#### Finish:
* some of the CGG problem.

#### Todo:
* there are too many new/free operations, need an object pool to enhance the overall performance.
* use one item(current is `MemoryLogItemLeader` && `AppendEntriesRequest`) to retain the request in `RaftServiceImpl::Write` .

# 2018-11-23
#### Finish:
* improve : add sleeping logic for update server status process to avoid potential issue.
* some of the CGG problem.

#### Todo:
* improve:
  * checking remote peer address .
  * ~~reduce mutex lifetime.~~

#### Note:
* `wait_for(lock,time,pred)` won't return event if it got a signal.If `pred` is specified , the
  only way to unblock is that `pred` get satisfied.

# 2018-11-21
#### Finish:
* `server status` logic.

#### Note:
*  an important constrain : leader can't **directly commit** entries from elder's term,these entries 
   can only be committed **together with the entries from the current term**.
* [this place](https://github.com/brpc/braft/blob/master/docs/cn/raft_protocol.md) has good 
  explanations(written in Chinese) toward some confusing part of raft , thumb up.

# 2018-11-20
#### Todo:
* ~~use a pending list for appending entries is no more an optimizing job but a feature request job.~~
* ~~correctly set `server status` on leader side.~~

# 2018-11-19
#### Finish:
* solution of CGG problem:
  * `GuidGenerator` has the ability of generating guid based on a prior one other than 
     it's neighbour .
  * add a swich defining whether the leader is now available or not:
    * turn off : any of the pending phaseI requests got majority failure.
    * turn on  : after process the request which satisfy condition : `>= [LRL] and <= [last issued guid]`.

* pros: easy to implement.
* cons: server will choke.

#### Todo:
* improve :
  * ~~use a memory pending list when multiple threads trying to append entries to the binlog file
    on the leader side thus reduce the overhead caused by the notifying operations.~~

# 2018-11-18
#### Note:
* about `Condition Variable`
  * multiple waiting threads can wait on the same cv variable , with different waiting conditions.
  * notifying doesn't need to hold the mutex , I think modification of the condition is that  too, if you don't have to consider condition synchronization .

# 2018-11-15
#### Finish:
* data-resync logic.
* ~~remove usage of `Exception`.~~

# 2018-11-14
#### Finish:
* bugfix:
  * incorrect use of `_p_buf` in `RaftCore.Leader.LeaderView.ReSyncLogCB `.
  * truncate size issue old binlog file .
  * dead lock issue when using `BinLogGlobal::m_instance.m_mutex_cv` in `BinLogGlobal::m_instance.AppendEntry` .
* some of the data-resync logic.

# 2018-11-13
#### Finish:
* some of the data-resync logic.
* bugfix:
  * issue in `earlier X entries` in `ReSyncLogCB`.
  * `ReSyncLogContext::m_last_sync_point` didn't update.
* improve:
  * use overloaded functions in `LogIdentifier` to solve the problem of `the default constructor of "std::atomic<LogIdentifier>" cannot be referenced -- it is a deleted function`.

#### Todo:
* ~~all implementations(class/struct...) move to `.cc` files.~~


# 2018-11-12
#### Finish:
* improve  `BinlogItem` structure: 
  * use `Entity`  instead of some tedious fields to represent a log entry's info.
  * remove `crc32` .I think it is useless to add a crc field inside a pb message.
  * clear the contents in `std::shared_ptr` objects of `ConnPoolEntry` class.
* some of the data-resync logic

#### Todo:
* code refactoring:
  * modular big functions in `leader_view.cc`
  * ~~move `using ::xxxx` to `.cc` files if feasible~~
* optimizing:
  * ~~use a conservative strategy for re-syncing log.Invoid triggering DATA-SYNC as far as possible.~~

# 2018-11-09
#### Finish:
* Reverting log logic in server side
* some bugfixs
* return connection entry back to the connection pool ASAP
* fetch the connection every time we need it

#### Todo:
* optimize :
  * node pool for `LockFreeDeque` && `LockFreeHash` && `TrivialLockDoubleList`
* ~~DATA-SYNC logic~~

#### Todo:
* ~~return connection entry back to the connection pool ASAP~~
* ~~fetch the connection every time we need it~~
* ~~CGG problem~~
* ~~follower left behind forever problem.~~

# 2018-11-07
#### Finish:
* some of the Reverting log logic in server side

#### Maintenance:
* Setted up a gitlab private repo 

# 2018-11-06
#### Finish:
* `LockFreePriotityQueue`

# 2018-11-04
#### Finish:
* working on priority queue solution

# 2018-11-03
#### Finish:
* still in discussion  of the previous election problem

# 2018-11-02
#### Finish:
* a little coding refactoring job.
* have a problem about candidate's behavior on election ,this is the [mail](https://mail.google.com/mail/u/0/#sent/FFNDWNNwPdQLQHpXjHvVHrLgpHQhdHxC)

# 2018-11-01
#### bugfix:
* issue in `LockFreeQueue<T>::Pop()`

# 2018-10-30
#### Finish:
* follower binlog reverting

#### Todo:
* ~~leader side reverting related logic~~

# 2018-10-29
#### Todo:
* ~~how to add CRC32 to `FileMetaData::IdxPair`~~

# 2018-10-28
#### Finish:
* add `REVERTING` status to binlog status on both leader & follower side to handle the log
 reverting case correctly.

# 2018-10-26
#### Finish:
* binlog rotating need to consider ID-LCL

# 2018-10-23
#### Finish:
* add `LockFreeHash::Delete()`by using a delete flag like `TrivialLockDoubleList`
* check the checksum when parsing binlog during startup

#### Bugfix:
* parsing issue in `FileMetaData::GenerateBuffer(uint32_t &buf_size)`
* parsing issue in `FileMetaData::ConstructMeta(std::FILE* _handler)`

#### Todo:
* ~~maintain ID-LCL~~
* ~~binlog rotating need to consider ID-LCL~~
* ~~log reverting in follower~~
* ~~CGG problem~~

#### Note:
* It's impossible for a follower to receive a log from the leader that conflict with its committed
 log : if is committed , the majority must have replicated it to their own logs. The new leader won't
 send a log conflicting with its own replicated log to any of its followers.

# 2018-10-22
#### Finish:
* About `std::condition_variable::wait_for()` [When unblocked, **regardless of the reason**, 
    lock is reacquired and wait_for() exits](https://en.cppreference.com/w/cpp/thread/condition_variable/wait_for).
    This is **a SUPER important guarantee for safety**. Upon this , we can safely do delete operations
    in `RaftServiceImpl::AppendEntries()` when waiting thread is timed out. What a great property of `std::condition_variable`!!!
* `TrivialLockDoubleList::Delete`  is done.

# 2018-10-16
#### Finish:
* a possible implementation for `TrivialLockDoubleList::Delete` : tag the element to be deleted, ignore
 the taged ones when iterating the list.

# 2018-10-15
#### Finish:
* `Log Replication` of raft is now basically finished
#### Todo:
* ~~solve CGG problem.~~

# 2018-09-25
#### Finish:
* improve :using `&`(and operator) and a 2^n number to replace `%`(mod operator)

# 2018-09-19
#### Finish:
* improve `ConnPoolEntry` implementation
* improve connection pool implementation:
  * add minimal size & security and the corresponding new rules
* replace `std::container::push_back()` with `emplace_back()` as much as possible
* **find a major defect** on  throughput : one thread **cannot** process the next client request 
  before thre previous is done!!! What horrible a mistake I made!!!! Too late to start
  all over again. Now there are two possible roads to deal with this :
  * increate #worker threads in gRPC to mitigate the influence of the mistake.
  * investigate how to implement an asynchronous gRPC server which may lead to code mess up.

#### Note:
* I feel so fucking stupid to ignore the `synchronous vs asynchronous` difference inside one
  thread. Holyshit... An alternative of libevent may be choosen if I realized the problem earlier,
  and implement a multi-thread fully asynchronous server side framework by hand.

# 2018-09-18
#### Note:
* `StatusCode` in gRPC :
  * connection fail : UNAVAILABLE = 14
  * read timeout : DEADLINE_EXCEEDED = 4

# 2018-09-17
#### Finish:
* designed a solution for the CGG problem : `guid` generated by the generator will never revert, 
 it just go ahead all the time,keep its  monotonicity.  The  based on `previous guid` revert.
* Follower  side finished implementing the above design.

#### Note:
* `std::condition_variable::wait(std::mutex>& lock, Predicate pred)`, will not return
   if the condition described by `pred` is not satisfied even though the CV itself is notified.(Tested
   on my win10 PC)
* `spurious wakeup` doesn't happen repeatly on my Win10 PC (what an unexpected thing!!!).


# 2018-09-16
#### Finish:
* `std::condition_variable::wait(std::mutex>& lock, Predicate pred)`, will also stop blocking
  if `pred` is satisfied , see [here](https://en.cppreference.com/w/cpp/thread/condition_variable/wait),
  guessing probably because of the spurious wakeup during which `pred` will be checked.

# 2018-09-15
#### Finish:
* add glossary.md to project
* New design:  in the Leader's implementation of coping iwth `implicitly fail`, `LRG`  should never been rolled back , instead,resign the `previous
 log ID` to the `ID-LRL` is a better approach.
* improve the waiting logic in follower's `AppendEntries` implementation.

#### Todo:
* ~~CGG problem on the leader side.~~

# 2018-09-12
#### Note:
* The `tag` variable returned by `CompletionQueue::Next(void **tag, bool *ok)` just indicates 
  that there is a deterministic result available ,doesn't represent the acutal RPC result(include 
  connect fail) which should instead be checked by the `::rpc::Statue` object . A more offical
  (also opaque) explanation is available [here](https://grpc.io/grpc/cpp/classgrpc_1_1_completion_queue.html#a86d9810ced694e50f7987ac90b9f8c1a).


# 2018-09-11
#### Finish:
* some of the replicating logic in `AppendEntries` RPC.

#### Note:
* be careful using `set_allocated_xx` member function in `::google::protobuf::Message`: It takes
  the ownership of the pointer,so it's better to invoke `release_xx` after the message's job is done and 
  before the object get destructed.

# 2018-09-08
#### Note:

* About gRPC:
  * `grpc::CreateChannel` and `NewStub` function in gRPC don't create a new connection,instead , 
  it is the actual RPC call who actually do that, this is already been checked by wireshark .
  * the server's asynchronous mode is very different from its synchronous mode.And I haven't find 
    how does it can process client requests concurrenty accroding to  [its example](https://github.com/ppLorins/grpc/blob/master/examples/cpp/helloworld/greeter_async_server.cc).
    The moment swtich to the asynchronous mode isn't coming.
  * whether or not can multiple stubs share the same channel is still unclear.I don't find any gRPC
    official docs have plain explainations on this subject. Many disscuss(like [this](https://github.com/grpc/grpc-go/issues/682)) 
    can be found in the mailing list and github project's issues, but none of them can be identified true or identified false.
  * how to maintain a long connectin between client and server is still unclear. I'm more and more
    dislike gRPC for reasons.

# 2018-09-05
#### finish:
* bugfix: `FileMetaData::ConstructMeta(std::FILE* _handler)` parsing file issue

# 2018-09-04
#### finish:
* implementation of `lockfree ringbuf queue`

# 2018-08-29
#### finish:
* made some progress of implementing `trivially lock queue`
* still need a separate `std::unordered_map` object to represent the index to 
   the lock free accessed data structure : array of producer threads' positions.

#### Note:
* some conclusions about the `volatile` keyword in c++11 :
  * make CPU operates directly on memory rather than registers(everyone knows).
  * compile order is guaranteed between mutiple volatile variables,but not among normal variables.
  * CPU exection order cannot be guaranteed by `volatile`.
  * the right way to ensure the correctness in multiple threading environment is to construct the `happens before` semantic
     manually. Both exclusive lock and atomic operations can achieve this.
  * `spurious wakeup` in condition variable and `spurious fail` in `std::atomic::compare_exchange_weak`
    could probably also the result of CPU reorder issue.
  
  
# 2018-08-27
#### finish
* read [an article about volatile](https://stackoverflow.com/questions/45663059/volatile-keyword-with-mutex-and-semaphores?rq=1),
  seems it is still needed is the lock free code.

# 2018-08-21
#### finish
* found it's not possible to allocate a 2^64*8 Bytes size memory block.Need `%` again.

# 2018-08-20
#### finish
* simple sketch of the multiple consumer multiple producer lock free class

# 2018-08-17
#### finish
* for the implementation of a multiple-producer-multiple-consumer lock free quque,  inspirations from a pretty good [article](https://www.linuxjournal.com/content/lock-free-multi-producer-multi-consumer-queue-ring-buffer) are  outlined here:
  *  using `std::mutex` and ￥`std::condition_variable` are expensive operations , they heavily invoke `futex` under linux resulting in a vast waste of CPU times.
  *  `yield` a thread  when it is waiting some certain conditions that are checked by a loop is pretty much acceptable in performance  than I imagined before.
  *  a better approach of achieving `(x+1) % size` is `x++ & _size` where `_size` is defined as `static const uint32_t _size = size - 1` and `size` is a power of 2
  *  checking the boundaries of multiple producers or multiple consumers is critical for correctness.

## 2018-08-15
#### finish:
* `CommitEntries`  interface
* bugfix: `ReleaseCuttedHead` issue in a  corner case

#### todo:
* ~~a multiple producer & multiple consumer (better lock-free) queue~~

## 2018-08-14
#### finish:
* bugfix: for loop issue in `AppendEntries`
* `CutHeadByValue` method in `TrivialLockDoubleList`

## 2018-08-13
#### finish:
* bugfix: update `m_last_replicated` issue in `AppendEntries`
* be sure that the two references of `m_last_replicated` is safe.

#### Note:
* seems there is no solution for parallelizing the `CutHead` method and the `CutBy` method.An 
  exclusive lock is needed.


## 2018-08-11
#### finish:
* fix a bug in `TrivialLockDoubleList` :redundant elements should not be allowed
* [a feature request](https://github.com/grpc/grpc/issues/16330) for fRPC aimed at performance enhancement
* `AppendEntries` done 

## 2018-08-06
#### finish:
* replace the `AppendRequest` in `BinLogOperator` with `::raft::Entity`
* thought out a method for reference k-v in request and at lease one copy is needed 

#### Todo:
* copy k-v out of the request probably in `MemoryLogItemFollower`

## 2018-08-05
#### finish:
* refactor:
  * classes hierarchy for node view
  * replace `::raft::EntityID` with `LogIdentifier` which is `triviallyCopable` 
    and can be used with `std::atomic`

* improve initialization for the whole process and node views


## 2018-08-04

#### Note:
* found the `set_allocated_field()` member function in ProtoBuf's Message class definition.It's
  a nice tool avioding memory copy for bytes like fields like `string`.

## 2018-08-03

#### Note:
* decided to give up the lock-free attempt for linked list. It can be achieved inspired by 
  [Harrirs's paper](http://www.cs.technion.ac.il/~erez/Papers/wfll.pdf)
  but will introduce notoriously complexity leading to a notable drawndown for the ratio of 
  inventment and production.

## 2018-07-31

#### Note:
* adding delete operations for `trivial lock list` and `trivial lock hash` is **HARD**
  ,I'm reading on [this paper](http://www.cs.technion.ac.il/~erez/Papers/wfll.pdf) seeking for a well demonstrated solution.

## 2018-07-29

#### finish:
* finish `AppendEntries` interface except for the 'reverting log' part which is literally boring.

#### todo:
* ~~design problem of `trivial lock list`: need delete operations but seems too hard to achieve.~~

## 2018-07-28

#### finish:
* found a compiler issue : the c++ compiler for MSVC2015(
`Microsoft (R) C/C++ Optimizing Compiler Version 19.00.24215.1 for x86`) doesn't conform
[this rule](https://en.cppreference.com/w/cpp/atomic/atomic):
![](https://c1.staticflickr.com/1/936/43642354582_4dfa7165aa_o.png) While `Apple LLVM version 7.0.2 (clang-700.1.81)` does.Then 
I can't give a user-provided constructor for `LogIdentifier`, disgusting. Already posted a comment [here](https://developercommunity.visualstudio.com/content/problem/101208/c-compiler-is-overly-strict-regarding-whether-a-cl.html?inRegister=true)
 under a similar issue report on the MSVC development forum.


#### todo:
* ~~design problem of `trivial lock list`: need delete operations but seems too hard to achieve.~~

#### Note:
* feel shamed for being absent for more than one month: I've been watching the WorldCup and being
lazy to start again even after that. I'm back afterwards, time to concentrate on this project .


## 2018-06-23

#### finish:
* code refactoring
   * combine the intersection of `LogIdentifier` and `FileMetaData::IdxPair`

#### todo:
* ~~`AppendEntries` interface implementation~~



## 2018-06-15:

#### finish:
* trivial lock list  `CutHead` method
* code refactoring
   * ~~simplify follower's representation in leader's view~~
   * improve namespace usage
   * improve code structure

#### note:
* template type of `std::atomic` must be [TriviallyCopyable](http://en.cppreference.com/w/cpp/named_req/TriviallyCopyable).
  This is the reason why we need `LogIdentifier` even we already have `BinlogItemEntity`.
* protobuf's `ByteSize()` method can be used to calculate the length of the serialized buffer without
  serializing.

## 2018-06-14:

#### finish:
* figured out the implementation of trivial lock list  `CutHead` method

#### note:
* until now , almost all lock-free operations are based on CAS

## 2018-06-09:

#### finish:
* trivial lock list  `Insert` method

#### note:
* [key diffference](https://stackoverflow.com/questions/4944771/stdatomic-compare-exchange-weak-vs-compare-exchange-strong) 
   between `compare_exchange_weak` and `compare_exchange_strong` is whether the 
   library handles the `spurious wakeup` or not

## 2018-06-07:

#### finish:

* trivial lock hash 
* file meta data appended at the end of each binlog file

#### todo:
* ~~trivial lock list~~

#### others:
 * after finish `trivial lock list ` I need to stop and do a lot of unit test by using gtest...

## 2018-06-05:

#### working on:
 * binlog file meta data manipulating functions,boring..

## 2018-06-04:

#### finish:
 * figured out an easier approach of implementing the on-disk data structure by using protobuf
 * a simple test over pb compression rate(approximately 63%):
 ```protobuf
message FileMeta {
  repeated int64 m1  = 1;
  repeated int32 m2  = 2;
}
 ```

## 2018-06-03:

#### finish:
* learned [a way](https://www.codeproject.com/Articles/48575/How-to-define-a-template-class-in-a-h-file-and-imp) 
 to separating template class member function definitions from its .h file into a coresponding .cpp file.


## 2018-05-27:

#### finish:
  * finish `trivial lock hash` 

#### questions:
 * Why a [compare_exchange_weak](https://en.cppreference.com/w/cpp/atomic/atomic/compare_exchange) operation can be divided into `read-modify-write` and `load`? 
   ![cas memory](https://c2.staticflickr.com/2/1757/42332496312_8d9cc0b9c6_o.png)

   > Updated at 2018-07-29 : Because they are for differenct operations: former is for successfully
   > CAS , later is for failed CAS.

## 2018-05-23:

#### finish:
* finish a little part of `trivial lock hash`
* learned new usage of `std::xx<T> {}(); ` temporary object lives only in this line

#### todo:
* ~~same as before.~~

## 2018-05-22:

#### finish:
* figured out a high performance solution for `trivial lock hash`:
  * use `std::atomic<T>::compare_exchange_weak()` to insert new nodes in a solt
  * memory size prepared for the hash solts need to be configurable(default to 10MB)
  * memory_order constrains in CAS:
    * `read-modify-write` operation :  `memory_order_acq_rel`
    * `store` operation : `memory_order_acquire`

* review c++11+ memory_orders:
  * release-consume  : loose
  * release-acquire  : strict
  * memory_order_acq_rel :for read-modify-write,strict
  * memory_order_seq_cst : rigid , all thread observe all modifications in the same order

#### todo:
* ~~trivial lock hash ~~
* ~~file meta data appended at the end of each binlog file~~
* ~~trivial lock list~~

## 2018-05-21:

#### finish:
 * figured out a solution for the wating timeout issue which occured in followers who write ahead binlog:
    * when wating threads need to stop waiting for that node ,set `waiting flag` to false and return 
      an implicit error to cliet
    * periodically purge nodes whose `waiting flag` is false
    * when inserting node to the list, need to be sure whether it has been inserted or writted to the 
      binlog file or not.

#### todo:
* ~~trivial lock list~~
* ~~file meta data appended at the end of each binlog file~~


## 2018-05-17:

#### finish:

 * clear about how kafka use the file system : [don't fear the file system](https://kafka.apache.org/documentation/#design).
   This is very suggestive for any disk heavy application's design.

 * learned how kafka locating old logs in `offset` mechanism.[View it here](https://kafka.apache.org/documentation/#implementation),
   or see this:![kafka offset](https://c1.staticflickr.com/1/943/40368256110_3ab07b8ac6_o.png)

 * design a new file format(file meta data appended to the end) to support follower's `log revert` when detect confilicting.
   When there are no match logs found in the looking back process,then the follower need to resync all data from
   the leader, as described in raft paper.

#### experiment:

  * it's safe to do multiple thread reading and writing for the same file, when they are operating different
    parts of the file.

  * simple benchmark of my SSD([transcend-ssd370s](https://www.cnet.com/products/transcend-ssd370s/specs/)) random writing on my win10 PC ,each thread writing different part of the same file:

    * 1 thread writing :  approximately **52000/second**(with and without std::flush).
    * 2 threads writing in parallel:approximately **106000/second**(with and without std::flush).
    * 3 threads writing in parallel:approximately **124000/second**(with and without std::flush).
    * 4 threads writing in parallel:approximately **132000/second**(with and without std::flush).
    * add 1 threads reading while multiple threads writing in parallel:  **doesn't impact performance** (That's a good news).

  * `std::cout` and `printf` are both heavily time consuming functions.So be sure of earsing them when doing benchmark.

#### todo :

  * ~~consider how to deal with `trivial lock list` timeout issue.~~

## 2018-05-13: 

#### finish:
* just finished a little part of trivial lock list

#### thoughts:
* I think I need [googletest](https://github.com/google/googletest) to do unit testing

## 2018-05-11: 

#### finish:

* basic defition of `Node` in `trivial lock list`  

* clear about that if multiple threads wating on a same condition variable's `cv.wait()` call, and another thread 
  finally call `cv.notify_all()` , **ONLY ONE** of the waiting threads can break out from the `cv.wait()` call ,and
  the rest ones break  one by one after the previous one release the mutex which cv depending on.
  Currently only `BinLogGlobal::m_instance.AppendEntry` use CV,I've checked that its usage is correct and safe.

* clear about that std::atomic **may** also be lock free when doing `load()` and `store()`, and this can be
  verified by `std::atomic<T>::is_lock_free()`

#### questions:

* Is accessing `void*` without lock safe in multi-threading environment under all 32 & 64 bit platforms? 
   **I believe it is true** but don't know how to prove. Take a look at the disassemble code in my win10 PC:
   ![disassemble](https://c1.staticflickr.com/1/953/40232734020_35c9b744ff_o.png)

#### todo:

  *  ~~implementing the `trivial lock list`~~

## 2018-05-10: 


#### finish:

* figured out a new approach of implementing the `trivial lock list`:
  *  pointer 3 doesn't use any lock
  *  pointer 5 using normal std::mutex
  *  boost::upgrade_lock is no longer needed now

#### todo:

  *  ~~implementing the `trivial lock list`~~

#### thoughts:
* about boost::upgrade_mutex(boost 1_64_0) : How does these variables work properly considering CPU cache?

![ab](https://c1.staticflickr.com/1/946/42016090211_9ffbfaa3f8_o.png)

* clear now : volatile helps **nothing** about guaranteeing the correctness of multiple thread accessing. 
  It is just a hint to CPU that some variable's value should be read from RAM everytime they are used rather 
  than using their register's value directly. 
* something good to know:
    * [c-volatile-variables-and-cache-memory](https://stackoverflow.com/questions/7872175/c-volatile-variables-and-cache-memory)
    * [does-volatile-qualifier-cancel-caching-for-this-memory](https://stackoverflow.com/questions/18550784/does-volatile-qualifier-cancel-caching-for-this-memory/18550834)


## 2018-05-09: 

#### finish:

* read source code of boost::upgrade_lock, know it inner machanism
* found heavily colliding when  running the same `lock-trylock-unlock-lock` loop (**unsolvable**)
* removed redundant boost library files in my home win10 machine, did some additional works to make it easier
  for importing boost libraries from VS2015 (After successfully built boost 1.64 on win10,its library files distribued among
  many different directories which made importing from VS2015 messy.Disgusting!).
* `ls /source/path/*pattern* | xargs -I{} cp -u {} /destination/path ` is a nice usage for `xargs`
   when the output of the previous command is not intended to be the first argument of the next command.

#### todo:
* ~~trivial_lock_list: how to correctly using lock between step3 and step5 ? ~~
 
#### thoughts:
* trying to take advantage of multiple threading as much as possible is **really** tricky.
* condition variable is everywhere inside some basic libraries like boost , is that the only or best way to
  do notifing ?


